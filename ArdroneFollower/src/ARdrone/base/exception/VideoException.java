package ARdrone.base.exception;

public class VideoException extends ARDroneException
{

	public VideoException(Throwable t)
	{
		super(t);
	}
}
