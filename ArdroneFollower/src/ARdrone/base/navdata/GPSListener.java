package ARdrone.base.navdata;

/**
 *
 * @author Friederich Pierre-André
 */

import java.util.EventListener;

public interface GPSListener extends EventListener {
    
    public void posUpdated(double latitude, double longitude, double elevation);
    
    public boolean isGPSactive(long gps_state);
}
