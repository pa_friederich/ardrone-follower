/**
 * The MIT License (MIT)
 * 
 * Copyright (c) 2013 Gautam Tandon (mail@gautamtandon.me)
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy 
 * of this software and associated documentation files (the "Software"), to deal 
 * in the Software without restriction, including without limitation the rights 
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell 
 * copies of the Software, and to permit persons to whom the Software is 
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in 
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE 
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN 
 * THE SOFTWARE.
 */

package ARdrone.base.geolocation.w3c;

/**
 * The <code>PositionCallback</code> interface provides a way for the <code>Geolocation</code> 
 * object to provide either a valid position using the <code>Position</code> class or an error 
 * using <code>PositionError</code> class. 
 */
public interface PositionCallback {
	
	/**
	 * Called by the <code>Geolocation</code> object to provide the position.
	 * 
	 * @param position	the position calculated by <code>Geolocation</code>.
	 */
	public void onSuccess(final Position position);
	
	/**
	 * Called by the <code>Geolocation</code> object to provide an error in case position was 
	 * unavailable.
	 * 
	 * @param positionError	the error that occured while trying to find the position.
	 */
	public void onFailure(final PositionError positionError);
}
