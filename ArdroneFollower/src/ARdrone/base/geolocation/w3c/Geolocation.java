/**
 * The MIT License (MIT)
 * 
 * Copyright (c) 2013 Gautam Tandon (mail@gautamtandon.me)
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy 
 * of this software and associated documentation files (the "Software"), to deal 
 * in the Software without restriction, including without limitation the rights 
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell 
 * copies of the Software, and to permit persons to whom the Software is 
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in 
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE 
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN 
 * THE SOFTWARE.
 */

package ARdrone.base.geolocation.w3c;

import java.util.Date;
import java.util.Hashtable;
import java.util.Timer;
import java.util.TimerTask;

import ARdrone.app.controlcenter.plugins.mapviewer.GeolocationAcquirer;

/**
 * <p>The <code>Geolocation</code> class provides Java implementation of the 
 * "Geolocation Interface" as detailed out in the W3C Specifications (
 * <a href="http://www.w3.org/TR/geolocation-API/#geolocation_interface">
 * http://www.w3.org/TR/geolocation-API/#geolocation_interface</a>).</p>
 * 
 * <p><b>Note: This class must not have any sub-classes to ensure W3C Specifications are being 
 * strictly followed by the system or application that uses this geolocation package.</b></p>
 */
public final class Geolocation {
	/*
	 * A hashtable of watch processes to maintain the watch processes currently running 
	 * along with their unique Ids.
	 */
	private Hashtable<Long, Thread> hashOfWatchProcesses;
	
	/*
	 * A hashtable of previous positions found by watch processes.
	 */
	private Hashtable<Long, Position> hashOfPreviousPositions;
	
	/*
	 * The thread that calculates (or "acquires") the position.
	 */
	private GeolocationAcquirer geolocationAcquirer;
	
	/**
	 * Protected constructor to ensure this object cannot be instantiated by any other class 
	 * outside of its package.
	 * 
	 * @param geolocationAcquirer	the thread that calculates (or "acquires") the position.
	 */
	protected Geolocation(final GeolocationAcquirer geolocationAcquirer) {
		this.geolocationAcquirer = geolocationAcquirer;
		this.hashOfWatchProcesses = new Hashtable<Long, Thread>();
		this.hashOfPreviousPositions = new Hashtable<Long, Position>();
	}

	/**
	 * Simply calls the <code>getCurrentPosition</code> function with 
	 * <code>PositionOptions</code> set to <code>null</code>.
	 * 
	 * @param callback	the callback object to be used to provide success or failure notification.  
	 */
	public void getCurrentPosition(final PositionCallback callback) {
		getCurrentPosition(callback, null);
	}
	
	/**
	 * <p>Immediately returns and then asynchronously attempts to obtain the current location of the 
	 * application. If the attempt is successful, the <code>onSuccess</code> method of the callback 
	 * object is invoked with a new Position object, reflecting the current location of the device. 
	 * If the attempt fails, the <code>onFailure</code> method of the callback object is invoked 
	 * with a new PositionError object, reflecting the reason for the failure.</p>
	 * 
	 * <p>For more details refer to W3C Specification <a 
	 * href="http://www.w3.org/TR/2012/PR-geolocation-API-20120510/#get-current-position">
	 * http://www.w3.org/TR/2012/PR-geolocation-API-20120510/#get-current-position</a></p>
	 * 
	 * @param callback	the callback object to be used to provide success or failure notification.
	 * @param options	the options to be used. These include things such as enabling high accuracy, 
	 * setting API call timeout.
	 */
	public void getCurrentPosition(final PositionCallback callback, final PositionOptions options) {
		long maximumAge = 0;
		if (options != null && options.getMaximumAge() >= 0) {
			maximumAge = options.getMaximumAge();
		} else {
			maximumAge = 0;
		}
		
		boolean isTimeoutInfinity = false;
		long timeout = 0;
		if (options != null) {
			if (options.getTimeout() >= 0) {
				timeout = options.getTimeout();
			} else {
				timeout = 0;
			}
		} else {
			isTimeoutInfinity = true;
		}
		
		boolean enableHighAccuracy = false;
		if (options != null) {
			enableHighAccuracy = options.isEnableHighAccuracy();
		} else {
			enableHighAccuracy = false;
		}
		
		if (geolocationAcquirer.getCachedPosition() != null) {
			long cachedPositionAge = System.currentTimeMillis() - geolocationAcquirer.getCachedPosition().getTimestamp();
			if (cachedPositionAge < maximumAge) {
				callback.onSuccess(geolocationAcquirer.getCachedPosition());
				return;
			}
		} else {
			// move on
		}
		
		if (!isTimeoutInfinity && timeout == 0) {
			callback.onFailure(new PositionError(PositionError.TIMEOUT));
			return;
		} else {
			// move on
		}
		
		geolocationAcquirer.setEnableHighAccuracy(enableHighAccuracy);
		geolocationAcquirer.setPositionCallback(callback);
		
		final Thread locationAcquirerThread = new Thread(geolocationAcquirer);
		if (!isTimeoutInfinity) {
			Timer timeoutTimer = new Timer(true);
			TimerTask timerTask = new TimerTask() {
				@Override
				public void run() {
					locationAcquirerThread.interrupt();
				}
			};
			timeoutTimer.schedule(timerTask, new Date(System.currentTimeMillis()+timeout));
		} else {
			// move on
		}
		locationAcquirerThread.start();
	}

	/**
	 * Simply calls the <code>watchPosition</code> function with <code>PositionOptions</code> set to 
	 * <code>null</code>.
	 * 
	 * @param callback	the callback object to be used to provide success or failure notification. 
	 * @return	a long value that uniquely identifies the watch operation.
	 */
	public long watchPosition(final PositionCallback callback) {
		return watchPosition(callback, null);
	}

	/**
	 * <p>Immediately returns a long value that uniquely identifies a watch operation and then asynchronously 
	 * starts the watch operation. It first attempts to obtain the current location of the device. If the 
	 * attempt is successful, the <code>onSuccess</code> method of the callback object is called with a new 
	 * <code>Position</code> object, reflecting the current location of the device. If the attempt fails, the 
	 * <code>onFailure</code> method of the callback object is called with appropriate error, reflecting the 
	 * reason for the failure. This method then continues to monitor the position of the device and invokes 
	 * the appropriate callback method every time this position changes. It continues until the <code>clearWatch
	 * </code> method is called with the corresponding identifier.</p>
	 * 
	 * <p>For more details refer to W3C Specification <a 
	 * href="http://www.w3.org/TR/geolocation-API/#watch-position">http://www.w3.org/TR/geolocation-API/#watch-position
	 * </a></p>
	 * 
	 * @param callback	the callback object to be used to provide success or failure notification.
	 * @param options	the options to be used. These include things such as enabling high accuracy, 
	 * setting API call timeout.
	 * @return	the long value that uniquely identifies this watch operation.
	 */
	public long watchPosition(final PositionCallback callback, final PositionOptions options) {
		boolean _isTimeoutInfinity = (options == null);
		final long watchId = System.currentTimeMillis();
		final boolean isTimeoutInfinity = _isTimeoutInfinity;
		
		Thread t = new Thread(new Runnable() {
			public void run() {
				while (true) {
					try {
						getCurrentPosition(new PositionCallback() {
							public void onSuccess(final Position position) {
								if (hashOfPreviousPositions.get(new Long(watchId)) == null) {
									hashOfPreviousPositions.put(new Long(watchId), position);
								} else {
									Position prevPos = hashOfPreviousPositions.get(new Long(watchId));
									
									double latDiff = Math.abs(prevPos.getCoords().getLatitude() - position.getCoords().getLatitude());
									double longDiff = Math.abs(prevPos.getCoords().getLongitude() - position.getCoords().getLongitude());
									double distDiff = Math.max(latDiff, longDiff);
									double accuracy = Math.abs(
											Math.max(
												prevPos.getCoords().getAccuracy(), 
												position.getCoords().getAccuracy()
												)
											);
									
									double altDiff = Math.abs(prevPos.getCoords().getAltitude() - position.getCoords().getAltitude());
									double altAccuracy = Math.abs(Math.max(
																	prevPos.getCoords().getAltitudeAccuracy(), 
																	position.getCoords().getAltitudeAccuracy()
																)
															);

									if (distDiff > accuracy || altDiff > altAccuracy || 
											prevPos.getCoords().getHeading() != position.getCoords().getHeading() || 
											prevPos.getCoords().getSpeed() != position.getCoords().getSpeed()) {
										hashOfPreviousPositions.put(new Long(watchId), position);
										callback.onSuccess(position);
									} else {
										// don't call the callback yet 
									}
								}
							}
							public void onFailure(final PositionError positionError) {
								hashOfPreviousPositions.remove(new Long(watchId));
								callback.onFailure(positionError);
							}
						}, options);
						if (isTimeoutInfinity) {
							break;
						} else {
							Thread.sleep(options.getTimeout());
						}
					} catch (InterruptedException ie) {
						hashOfWatchProcesses.remove(new Long(watchId));
						hashOfPreviousPositions.remove(new Long(watchId));
					}
				}
				
				hashOfWatchProcesses.remove(new Long(watchId));
				hashOfPreviousPositions.remove(new Long(watchId));
			}
		});
		t.start();
		
		hashOfWatchProcesses.put(new Long(watchId), t);
		return watchId;
	}

	/**
	 * First checks the value of the given watchId argument. If this value does not correspond to any previously started 
	 * watch process, it returns immediately without taking any further action. Otherwise, the watch process identified by 
	 * the watchId argument is immediately stopped and no further callbacks are invoked.
	 * 
	 * @param watchId	the id that uniquely identifies the watch process that needs to be stopped.
	 */
	public void clearWatch(final long watchId) {
		Long key = new Long(watchId);
		Thread t = hashOfWatchProcesses.get(key);
		if (t != null) {
			t.interrupt();
			hashOfWatchProcesses.remove(key);
			hashOfPreviousPositions.remove(key);
		}
	}
}
