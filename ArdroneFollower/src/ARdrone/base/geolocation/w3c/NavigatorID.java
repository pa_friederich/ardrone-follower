/**
 * The MIT License (MIT)
 * 
 * Copyright (c) 2013 Gautam Tandon (mail@gautamtandon.me)
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy 
 * of this software and associated documentation files (the "Software"), to deal 
 * in the Software without restriction, including without limitation the rights 
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell 
 * copies of the Software, and to permit persons to whom the Software is 
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in 
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE 
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN 
 * THE SOFTWARE.
 */

package ARdrone.base.geolocation.w3c;

/**
 * The <code>NavigatorID</code> class provides Java implementation of the 
 * "NavigatorID Interface" as detailed out in the W3C Specifications (
 * <a http://www.w3.org/TR/2013/CR-html5-20130806/webappapis.html#navigatorid">
 * http://www.w3.org/TR/2013/CR-html5-20130806/webappapis.html#navigatorid</a>).
 */
public interface NavigatorID {
	
	/**
	 * Must return full name of the application, e.g. "My Test Application".
	 * 
	 * @return	the application name.
	 */
	public String getAppName();

	/**
	 * Must return either the string "4.0" or a string representing the version 
	 * of the application in detail, e.g. "1.0 (VMS; en-US) Mellblomenator/9000".
	 * 
	 * @return	the application version.
	 */
	public String getAppVersion();
	
	/**
	 * Must return either the empty string or a string representing the platform on 
	 * which the application is executing, e.g. "MacIntel", "Win32", "FreeBSD i386", 
	 * "WebTV OS".
	 * 
	 * @return	name of the platform on which the application is running.
	 */
	public String getPlatform();
	
	/**
	 * Must return the string "Gecko".
	 * 
	 * @return	the string "Gecko".
	 */
	public String getProduct();

	/**
	 * Must return empty string because this Java Implementation is written keeping in 
	 * mind its use in non-browser applications.
	 * 
	 * @return	an empty string.
	 */
	public String getUserAgent();
}
