/**
 * The MIT License (MIT)
 * 
 * Copyright (c) 2013 Gautam Tandon (mail@gautamtandon.me)
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy 
 * of this software and associated documentation files (the "Software"), to deal 
 * in the Software without restriction, including without limitation the rights 
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell 
 * copies of the Software, and to permit persons to whom the Software is 
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in 
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE 
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN 
 * THE SOFTWARE.
 */

package ARdrone.base.geolocation.w3c;

/**
 * <p>The <code>NavigatorStorageUtils</code> class provides Java implementation of the 
 * "NavigatorStorageUtils Interface" as detailed out in the W3C Specifications (
 * <a http://www.w3.org/TR/2013/CR-html5-20130806/webappapis.html#navigatorstorageutils">
 * http://www.w3.org/TR/2013/CR-html5-20130806/webappapis.html#navigatorstorageutils</a>).</p>
 * 
 * <p>This interface has been provided to comply with the W3C specifications and is not really 
 * required to be implemented because it makes sense for web browsers.</p>
 */
public interface NavigatorStorageUtils {
	public void yieldForStorageUpdates();
}
