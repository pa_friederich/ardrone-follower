/**
 * The MIT License (MIT)
 * 
 * Copyright (c) 2013 Gautam Tandon (mail@gautamtandon.me)
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy 
 * of this software and associated documentation files (the "Software"), to deal 
 * in the Software without restriction, including without limitation the rights 
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell 
 * copies of the Software, and to permit persons to whom the Software is 
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in 
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE 
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN 
 * THE SOFTWARE.
 */

package ARdrone.base.geolocation.w3c;

/**
 * <p>The <code>NavigatorGeolocation</code> class provides Java implementation of the 
 * "NavigatorGeolocation Interface" as detailed out in the W3C Specifications (
 * <a http://www.w3.org/TR/geolocation-API/#navi-geo">http://www.w3.org/TR/geolocation-API/#navi-geo</a>
 * ).</p>
 * 
 * <p>The class implementing this interface must be responsible for maintaining the <code>Geolocation</code> 
 * object and returning it to the calling thread upon request. The underlying logic of how the <code>
 * Geolocation</code> object is maintained is entirely upto the implementing class. For instance it may 
 * choose to create a new instance of the <code>Geolocation</code> object on every request, or maintain a 
 * single object and return the same every time.</p>
 */
public interface NavigatorGeolocation {
	public Geolocation getGeolocation();
}
