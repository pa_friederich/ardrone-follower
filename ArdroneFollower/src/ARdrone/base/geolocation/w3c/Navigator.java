/**
 * The MIT License (MIT)
 * 
 * Copyright (c) 2013 Gautam Tandon (mail@gautamtandon.me)
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy 
 * of this software and associated documentation files (the "Software"), to deal 
 * in the Software without restriction, including without limitation the rights 
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell 
 * copies of the Software, and to permit persons to whom the Software is 
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in 
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE 
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN 
 * THE SOFTWARE.
 */

package ARdrone.base.geolocation.w3c;

import ARdrone.app.controlcenter.plugins.mapviewer.GeolocationAcquirer;

/**
 * <p>The <code>Navigator</code> class provides Java implementation of the 
 * "Navigator Interface" as detailed out in the W3C Specifications (
 * <a href="http://www.w3.org/TR/2013/CR-html5-20130806/webappapis.html#navigator">
 * http://www.w3.org/TR/2013/CR-html5-20130806/webappapis.html#navigator</a>). As per the 
 * specification the <code>Navigator</code> class must also implement the <code>
 * NavigatorGeolocation</code> interface. An instance of <code>NavigatorGeolocation</code> 
 * would be then obtained by using binding-specific casting methods on an instance of 
 * <code>Navigator</code>.</p>
 * 
 * <p><b>Note: This class must not have any sub-classes to ensure W3C Specifications are being 
 * strictly followed by the system or application that uses this geolocation package.</b></p>
 */
public final class Navigator implements NavigatorID, NavigatorLanguage, NavigatorOnLine, 
										NavigatorContentUtils, NavigatorStorageUtils, 
										NavigatorGeolocation {

	private Geolocation geolocation = null;
	
	public Navigator(final GeolocationAcquirer geoAcquirer) {
		geolocation = new Geolocation(geoAcquirer);
	}
	
	public Geolocation getGeolocation() {
		return geolocation;
	}

	public void yieldForStorageUpdates() {
		throw new UnsupportedOperationException();
	}

	public void registerProtocolHandler(String scheme, String url, String title) {
		throw new UnsupportedOperationException();
	}

	public void registerContentHandler(String mimeType, String url, String title) {
		throw new UnsupportedOperationException();
	}

	public String isProtocolHandlerRegistered(String scheme, String url) {
		throw new UnsupportedOperationException();
	}

	public String isContentHandlerRegistered(String mimeType, String url) {
		throw new UnsupportedOperationException();
	}

	public void unregisterProtocolHandler(String scheme, String url) {
		throw new UnsupportedOperationException();
	}

	public void unregisterContentHandler(String mimeType, String url) {
		throw new UnsupportedOperationException();
	}

	public boolean isOnLine() {
		throw new UnsupportedOperationException();
	}

	public String getLanguage() {
		throw new UnsupportedOperationException();
	}

	public String getAppName() {
		throw new UnsupportedOperationException();
	}

	public String getAppVersion() {
		throw new UnsupportedOperationException();
	}

	public String getPlatform() {
		throw new UnsupportedOperationException();
	}

	public String getProduct() {
		throw new UnsupportedOperationException();
	}

	public String getUserAgent() {
		throw new UnsupportedOperationException();
	}
}
