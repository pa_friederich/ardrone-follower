/**
 * The MIT License (MIT)
 * 
 * Copyright (c) 2013 Gautam Tandon (mail@gautamtandon.me)
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy 
 * of this software and associated documentation files (the "Software"), to deal 
 * in the Software without restriction, including without limitation the rights 
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell 
 * copies of the Software, and to permit persons to whom the Software is 
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in 
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE 
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN 
 * THE SOFTWARE.
 */

package ARdrone.base.geolocation.w3c;

/**
 * <p>The <code>NavigatorContentUtils</code> class provides Java implementation of the 
 * "NavigatorContentUtils Interface" as detailed out in the W3C Specifications (
 * <a href="http://www.w3.org/TR/2013/CR-html5-20130806/webappapis.html#navigatorcontentutils">
 * http://www.w3.org/TR/2013/CR-html5-20130806/webappapis.html#navigatorcontentutils</a>).</p>
 * 
 * <p>The class implementing this interface may do whatever it likes when the methods are called. 
 * It could, for instance, prompt the user and offer the user the opportunity to add the site to 
 * a shortlist of handlers, or make the handlers his default, or cancel the request. It could provide 
 * such a UI through modal UI or through a non-modal transient notification interface. It could also 
 * simply silently collect the information, providing it only when relevant to the user.</p>
 * 
 * <p>The implementing class should keep track of which sites have registered handlers (even if the 
 * user has declined such registrations) so that the user is not repeatedly prompted with the same 
 * request.</p>
 */
public interface NavigatorContentUtils {
	public void registerProtocolHandler(String scheme, String url, String title);
	public void registerContentHandler(String mimeType, String url, String title);
	public String isProtocolHandlerRegistered(String scheme, String url);
	public String isContentHandlerRegistered(String mimeType, String url);
	public void unregisterProtocolHandler(String scheme, String url);
	public void unregisterContentHandler(String mimeType, String url);
}
