/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ARdrone.base.command;

/**
 *
 * @author pa_friederich
 */
public enum NavDataOptions {
    CKS_TAG(-1), DEMO_TAG(0), TIME_TAG(1), RAW_MEASURES_TAG(2), 
    PHYS_MEASURES_TAG(3), GYROS_OFFSETS_TAG (4), EULER_ANGLES_TAG (5),
    REFERENCES_TAG (6), TRIMS_TAG (7), RC_REFERENCES_TAG (8), PWM_TAG (9),
    ALTITUDE_TAG (10), VISION_RAW_TAG (11), VISION_OF_TAG (12), VISION_TAG (13),
    VISION_PERF_TAG (14), TRACKERS_SEND_TAG (15), VISION_DETECT_TAG (16),
    WATCHDOG_TAG (17), ADC_DATA_FRAME_TAG (18), VIDEO_STREAM_TAG (19), 
    GAMES_TAG (20), PRESSURE_RAW_TAG (21), MAGNETO_TAG (22), WIND_TAG (23),
    KALMAN_PRESSURE_TAG (24), HDVIDEO_STREAM_TAG (25), WIFI_TAG (26), 
    ZIMMU_3000_TAG (27);
    private final int value;
    
    private NavDataOptions(int value){
        this. value = value;
    }
    
    public int getValue(){
        return this.value;
    }
}
