package ARdrone.base.command;

public enum VideoBitRateMode {
	DISABLED, DYNAMIC, MANUAL;
}