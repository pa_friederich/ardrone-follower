package ARdrone.app.controlcenter.plugins.running;

import ARdrone.base.IARDrone;
import ARdrone.base.navdata.Altitude;
import ARdrone.base.navdata.AltitudeListener;
import ARdrone.base.navdata.AttitudeListener;
import ARdrone.base.navdata.TrackerData;
import ARdrone.base.navdata.VisionData;
import ARdrone.base.navdata.VisionListener;
import ARdrone.base.navdata.VisionPerformance;
import ARdrone.base.navdata.VisionTag;
import Jama.Matrix;

/**
 *
 * @author Friederich Pierre-André
 */

public class VisionController implements VisionListener, AttitudeListener, AltitudeListener {

    protected IARDrone drone;
    protected boolean doStop = false;
    static final double EPS_X = 1.0;
    static final double EPS_Z = 2.0;
    static final double EPS_YAW = 0.0;
    static final double GOAL_X = -100.0;
    static final double GOAL_Y = 0.0;
    static final double GOAL_Z = -150.0;
    static final double GOAL_YAW = 0.0;

    ARdrone.app.controlcenter.plugins.vision.PID PIDX = new ARdrone.app.controlcenter.plugins.vision.PID(0.001, 0.0, 0.0012);
    ARdrone.app.controlcenter.plugins.vision.PID PIDZ = new ARdrone.app.controlcenter.plugins.vision.PID(0.01, 0.0, 0.00705);
    ARdrone.app.controlcenter.plugins.vision.PID PIDYAW = new ARdrone.app.controlcenter.plugins.vision.PID(0.8, 0.0, 0.6);

    protected int xc;
    protected int yc;
    protected int distance;
    protected double altitude;
    protected double roll;
    protected double pitch;
    protected double yaw;
    protected double cx;
    protected double cz;
    protected double cyaw;
    float speedX = 0f;
    float speedZ = 0f;
    float speedYaw = 0f;
    protected Matrix P_d = new Matrix(3, 1);
    protected Matrix P_o = new Matrix(3, 1);
    double[][] r_dc = {{0, 1, 0}, {0, 0, -1}, {-1, 0, 0}};
    protected Matrix R_dc = new Matrix(r_dc);
    double[][] r_do = {{Math.cos(pitch), Math.sin(roll) * Math.sin(pitch), Math.cos(roll) * Math.sin(pitch)},
    {0, Math.cos(roll), -Math.sin(roll)},
    {-Math.sin(pitch), Math.cos(pitch) * Math.sin(roll), Math.cos(roll) * Math.cos(pitch)}};
    protected Matrix R_do = new Matrix(r_do);

    protected VisionTag[] tags = null;

    protected ARdrone.app.controlcenter.plugins.vision.Camera camera = new ARdrone.app.controlcenter.plugins.vision.Camera();

    public VisionController(IARDrone ardrone) {
        this.drone = ardrone;
    }

    protected void VisionProcessNavdata() {
        xc = tags[0].getX();
        yc = tags[0].getY();
        distance = tags[0].getDistance();
        camera.pixelToCentimeters(xc, yc, distance);
        P_d = R_dc.inverse().times(camera.P);
        P_o = R_do.inverse().times(P_d);
        yaw = Math.atan(P_o.get(1, 0) / P_o.get(0, 0));
    }

    protected void VisionControl() {
        double ex;
        double eyaw;
        VisionProcessNavdata();
        ex = P_o.get(0, 0) - GOAL_X;
        eyaw = -Math.atan(P_o.get(1, 0) / P_o.get(0, 0)) - GOAL_YAW;
        if (Math.abs(ex) < EPS_X && Math.abs(eyaw) < EPS_YAW) {
            speedX = 0f;
            speedYaw = 0f;
            PIDX.resetPID();
            PIDYAW.resetPID();
        } else {
            if (Math.abs(ex) > EPS_X) {
                double ux = PIDX.PIDgetCommand(ex);
                cx = limit(ux, -1.0, 1.0);
                speedX = Math.abs(cx) > 0.01 ? (float) cx : 0f;
            } else {
                cx = 0;
                speedX = 0f;
                PIDX.resetPID();
            }
            if (Math.abs(eyaw) > EPS_YAW) {
                double uyaw = PIDYAW.PIDgetCommand(eyaw);
                cyaw = limit(uyaw, -1.0, 1.0);
                speedYaw = Math.abs(cyaw) > 0.01 ? (float) cyaw : 0f;
            } else {
                cyaw = 0;
                speedYaw = 0f;
                PIDYAW.resetPID();
            }
        }
    }

    private double limit(double f, double min, double max) {
        return (f > max ? max : (f < min ? min : f));
    }

    public void stopController() {
        doStop = true;
    }

    @Override
    public void tagsDetected(VisionTag[] tags) {
        this.tags = tags;
        VisionControl();
    }

    @Override
    public void trackersSend(TrackerData trackersData) {

    }

    @Override
    public void receivedPerformanceData(VisionPerformance d) {

    }

    @Override
    public void receivedRawData(float[] vision_raw) {

    }

    @Override
    public void receivedData(VisionData d) {

    }

    @Override
    public void receivedVisionOf(float[] of_dx, float[] of_dy) {

    }

    @Override
    public void typeDetected(int detection_camera_type) {

    }

    @Override
    public void attitudeUpdated(float pitch, float roll, float yaw) {
        this.roll = roll;
        this.pitch = pitch;
        //this.yaw = yaw;
    }

    @Override
    public void attitudeUpdated(float pitch, float roll) {

    }

    @Override
    public void windCompensation(float pitch, float roll) {

    }

    int i = 0;

    @Override
    public void receivedAltitude(int altitude) {
        this.altitude = -altitude / 10.0;
        double ez = this.altitude - GOAL_Z;
        i++;
        if (i == 6) {
            if (Math.abs(ez) < EPS_Z && speedX != 0f && speedYaw != 0f) {
                cz = 0;
                this.drone.hover();
                PIDZ.resetPID();
            } else {
                double uz = PIDZ.PIDgetCommand(ez);
                cz = limit(uz, -1.0, 1.0);
                speedZ = Math.abs(cz) > 0.01 ? (float) cz : 0f;
                this.drone.getCommandManager().move(0f, speedX, speedZ, speedYaw);
                speedX = 0f;
                speedYaw = 0f;
            }
            i = 0;
        }
    }

    @Override
    public void receivedExtendedAltitude(Altitude d) {

    }
}

//public class VisionController extends Thread implements VisionListener, AttitudeListener, AltitudeListener {
//
//    protected IARDrone drone;
//    protected boolean doStop = false;
//    static final double EPS_X = 1.0;
//    //static final double EPS_Y = 5.0;
//    static final double EPS_Z = 0.0;
//    static final double EPS_YAW = 0.0;
//    private final static int SLEEP = 30;
//
//    static final double goalX = -200.0;
//    static final double goalY = 0.0;
//    //static final double goalZ = 0.0;
//    //static final double goalZ = -120.0;
//    static final double goalZ = -160.0;
//    static final double goalYaw = 0.0;
//
//    //PID PIDX = new PID(0.001, 0.0, 0.5);
//    PID PIDX = new PID(0.001, 0.0, 0.0012);
//    //PID PIDY = new PID(0.001, 0.0, 0.0); // unused
//    //PID PIDZ = new PID(0.0105, 0.0, 0.006);
//    PID PIDZ = new PID(0.01, 0.0, 0.00705);
//    //PID PIDYAW = new PID(0.8, 1.0E-6, 0.6); // ok, maybe a bit strong
//    PID PIDYAW = new PID(0.8, 0.0, 0.6);
//
//    protected int xc;
//    protected int yc;
//    protected int distance;
//    protected double altitude;
//    protected double roll;
//    protected double pitch;
//    protected double yaw;
//    protected double cx;
//    //protected double cy;
//    protected double cz;
//    protected double cyaw;
//    protected Matrix P_d = new Matrix(3,1);
//    protected Matrix P_o = new Matrix(3,1);
//    double [][] r_dc = {{0, 1, 0}, {0, 0, -1},{-1, 0, 0}};
//    protected Matrix R_dc = new Matrix(r_dc);
//    double [][] r_do = {{Math.cos(pitch), Math.sin(roll)*Math.sin(pitch), Math.cos(roll)*Math.sin(pitch)},
//        {0, Math.cos(roll), -Math.sin(roll)}, 
//        {-Math.sin(pitch), Math.cos(pitch)*Math.sin(roll), Math.cos(roll)*Math.cos(pitch)}};
//    protected Matrix R_do = new Matrix(r_do);
//
//    protected VisionTag[] tags = null;
//
//    protected Camera camera = new Camera();
//
//    public VisionController(IARDrone ardrone) {
//        this.drone = ardrone;
//    }
//
//    protected void VisionProcessNavdata() {
//        if (tags != null) {
//            xc = tags[0].getX();
//            yc = tags[0].getY();
//            distance = tags[0].getDistance();
//            camera.pixelToCentimeters(xc, yc, distance);
//            P_d = R_dc.inverse().times(camera.P);
//            P_o = R_do.inverse().times(P_d);
//            yaw = Math.atan(P_o.get(1, 0)/P_o.get(0, 0));
//        }
//    }
//
//    protected void VisionControl() {
//        double ex;
//        //double ey;
//        double ez;
//        double eyaw;
//        if(tags != null){
//            VisionProcessNavdata();
//            ex = P_o.get(0, 0) - goalX;
//            //ey = P_o.get(1, 0) - goalY;
//            //ez = P_o.get(2, 0) - goalZ;
//            eyaw = -Math.atan(P_o.get(1, 0)/P_o.get(0, 0)) - goalYaw;
//        }
//        else {
//            ex = 0;
//            eyaw = 0;
//        }
//        ez = altitude - goalZ;
//        
//        //if (Math.abs(ex) < EPS_X && Math.abs(ey) < EPS_Y && Math.abs(ez) < EPS_Z && Math.abs(eyaw) < EPS_YAW) {
//        if (Math.abs(ex) < EPS_X && Math.abs(ez) < EPS_Z && Math.abs(eyaw) < EPS_YAW) {
//            this.drone.hover();
//            cx = 0;
//            //cy = 0;
//            cz = 0;
//            cyaw = 0;
//            PIDX.resetPID();
//            //PIDY.resetPID();
//            PIDZ.resetPID();
//            PIDYAW.resetPID();
//        } 
//        else {
//            if (Math.abs(ex) > EPS_X) {
//                double ux = PIDX.PIDgetCommand(ex);
//                cx = limit(ux, -1.0, 1.0);
//            } else {
//                cx = 0;
//                PIDX.resetPID();
//            }
//            /*if (Math.abs(ey) > EPS_Y) {
//                double uy = PIDY.PIDgetCommand(ey);
//                cy = limit(uy, -1.0, 1.0);
//            } else {
//                cy = 0;
//                PIDY.resetPID();
//            }*/
//            if (Math.abs(ez) > EPS_Z) {
//                double uz = PIDZ.PIDgetCommand(ez);
//                cz = limit(uz, -1.0, 1.0);
//            } else {
//                cz = 0;
//                PIDZ.resetPID();
//            }
//            
//            if(Math.abs(eyaw) > EPS_YAW){
//                double uyaw = PIDYAW.PIDgetCommand(eyaw);
//                cyaw = limit(uyaw, -1.0, 1.0);
//            }
//            else {
//                cyaw = 0;
//                PIDYAW.resetPID();
//            }
//                
//
//            //if (Math.abs(cx) > 0.01 || Math.abs(cy) > 0.01 || Math.abs(cz) > 0.01 || Math.abs(cyaw) > 0.01) {
//            if (Math.abs(cx) > 0.01 || Math.abs(cz) > 0.01 || Math.abs(cyaw) > 0.01) {
//                float speedX = Math.abs(cx) > 0.01 ? (float)cx : 0;
//                //float speedY = Math.abs(cy) > 0.01 ? (float)cy : 0;
//                float speedZ = Math.abs(cz) > 0.01 ? (float)cz : 0;
//                float speedYaw = Math.abs(cyaw) > 0.01 ? (float)cyaw : 0;
//                this.drone.getCommandManager().move(0f, speedX, speedZ, speedYaw);
//                //this.drone.getCommandManager().move(0f, speedX, 0f, 0f);
//                //this.drone.getCommandManager().move(0f, 0f, speedZ, 0f);
//                //this.drone.getCommandManager().move(0f, 0f, 0f, speedYaw);
//                //this.drone.getCommandManager().move(0f, 0f, speedZ, speedYaw);
//            }
//        }
//    }
//
//    private double limit(double f, double min, double max) {
//        return (f > max ? max : (f < min ? min : f));
//    }
//
//    public void stopController() {
//        doStop = true;
//    }
//
//    @Override
//    public void tagsDetected(VisionTag[] tags) {
//        this.tags = tags;
//    }
//
//    @Override
//    public void trackersSend(TrackerData trackersData) {
//
//    }
//
//    @Override
//    public void receivedPerformanceData(VisionPerformance d) {
//
//    }
//
//    @Override
//    public void receivedRawData(float[] vision_raw) {
//
//    }
//
//    @Override
//    public void receivedData(VisionData d) {
//
//    }
//
//    @Override
//    public void receivedVisionOf(float[] of_dx, float[] of_dy) {
//
//    }
//
//    @Override
//    public void typeDetected(int detection_camera_type) {
//
//    }
//
//    @Override
//    public void attitudeUpdated(float pitch, float roll, float yaw) {
//        this.roll = roll;
//        this.pitch = pitch;
//        //this.yaw = yaw;
//    }
//
//    @Override
//    public void attitudeUpdated(float pitch, float roll) {
//
//    }
//
//    @Override
//    public void windCompensation(float pitch, float roll) {
//
//    }
//    
//    @Override
//    public void receivedAltitude(int altitude) {
//        this.altitude = -altitude/10.0;
//    }
//
//    @Override
//    public void receivedExtendedAltitude(Altitude d) {
//        
//    }
//
//    @Override
//    public void run() {
//        while (!doStop) // control loop
//        {
//            try {
//                tagsDetected(tags);
//                VisionControl();
//                tags = null;
//            } catch (Exception exc) {
//                exc.printStackTrace();
//            }
//            try {
//                Thread.sleep(SLEEP);
//            } catch (InterruptedException ex) {
//                Logger.getLogger(VisionController.class.getName()).log(Level.SEVERE, null, ex);
//            }
//        }
//    }
//}
