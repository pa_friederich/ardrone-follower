package ARdrone.app.controlcenter.plugins.running;

import Jama.Matrix;

/**
 *
 * @author Friederich Pierre-André
 */
public class Camera {

    double[][] ic = {
        {1115.6, 0.0, 614.9},
        {0.0, 1112.6, 420.1},
        {0.0, 0.0, 1.0}};
    Matrix Ic = new Matrix(ic);
    Matrix P = new Matrix(3, 1);
    Matrix p = new Matrix(3, 1);

    private final double xRatio = 1.28; // 1280/1000
    private final double yRatio = 0.72; //  720/1000

    /**
     *
     * @param xPixel the x position of the tag in the 1000x1000 square
     * @param yPixel the y position of the tag in the 1000x1000 square
     * @param distance the estimated distance from camera to the tag in centimeters
     */
    public void pixelToCentimeters(int xPixel, int yPixel, int distance) {

        this.p.set(0, 0, xPixel * xRatio);
        this.p.set(1, 0, yPixel * yRatio);
        this.p.set(2, 0, 1);

        // p in camera space -> P
        P = Ic.inverse().times(this.p).times(distance);
    }

    /**
     *
     * @return the x position in the camera space
     */
    public double getX() {
        return this.P.get(0, 0);
    }

    /**
     *
     * @return the y position in the camera space
     */
    public double getY() {
        return this.P.get(1, 0);
    }

    /**
     *
     * @return the z position in the camera space
     */
    public double getZ() {
        return this.P.get(2, 0);
    }
}
