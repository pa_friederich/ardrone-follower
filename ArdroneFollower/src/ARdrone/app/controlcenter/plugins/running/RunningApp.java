/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ARdrone.app.controlcenter.plugins.running;

import ARdrone.app.controlcenter.CCPropertyManager;
import ARdrone.app.controlcenter.ICCPlugin;
import ARdrone.app.controlcenter.plugins.vision.VisionVideoCanvas;
import ARdrone.base.IARDrone;
import ARdrone.base.command.CommandManager;
import ARdrone.base.command.DetectionType;
import ARdrone.base.command.EnemyColor;
import ARdrone.base.command.LEDAnimation;
import ARdrone.base.command.VideoCodec;
import ARdrone.base.command.VisionTagType;
import ARdrone.base.configuration.ConfigurationManager;
import ARdrone.base.exception.ARDroneException;
import ARdrone.base.exception.CommandException;
import ARdrone.base.exception.ConfigurationException;
import ARdrone.base.exception.IExceptionListener;
import ARdrone.base.exception.NavDataException;
import ARdrone.base.exception.VideoException;
import ARdrone.base.geolocation.w3c.Coordinates;
import ARdrone.base.geolocation.w3c.Position;
import ARdrone.base.navdata.GPSListener;
import ARdrone.base.navdata.NavDataManager;
import ARdrone.base.navdata.VisionTag;
import ARdrone.base.video.ImageListener;
import ARdrone.base.video.VideoManager;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;
import java.awt.geom.Point2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import org.alternativevision.gpx.GPXParser;
import org.alternativevision.gpx.beans.GPX;
import org.alternativevision.gpx.beans.Track;
import org.jxmapviewer.JXMapKit;
import org.jxmapviewer.JXMapViewer;
import org.jxmapviewer.OSMTileFactoryInfo;
import org.jxmapviewer.VirtualEarthTileFactoryInfo;
import org.jxmapviewer.viewer.DefaultTileFactory;
import org.jxmapviewer.viewer.GeoPosition;
import org.jxmapviewer.viewer.TileFactory;
import org.jxmapviewer.viewer.TileFactoryInfo;
import org.jxmapviewer.viewer.Waypoint;
import org.jxmapviewer.viewer.WaypointPainter;
import org.xml.sax.SAXException;

/**
 *
 * @author Friederich Pierre-André
 */
public class RunningApp extends JPanel implements ICCPlugin, ImageListener {

    public final static String FORMAT_MP4 = "MPEG-4";
    public final static String FORMAT_H264 = "H.264";
    private static final String GPXCREATOR = "ARDroneFollower";
    private static final String GPXVERSION = "1.1";
    private static final SimpleDateFormat GPXDATEFORMAT
            = new SimpleDateFormat("yyyy-MM-dd_HHmm", Locale.US);
    static final String DATEFORMAT = "yyyy-MM-dd HH:mm:ss";
    private static Icon greenIcon;
    private static Icon redIcon;

    public static Date StringDateToDate(String StrDate) {
        Date dateToReturn = null;
        SimpleDateFormat dateFormat = new SimpleDateFormat(DATEFORMAT);

        try {
            dateToReturn = (Date) dateFormat.parse(StrDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return dateToReturn;
    }

    public static Date GetUTCdatetimeAsDate() {
        return StringDateToDate(GetUTCdatetimeAsString());
    }

    public static String GetUTCdatetimeAsString() {
        final SimpleDateFormat sdf = new SimpleDateFormat(DATEFORMAT);
        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
        final String utcTime = sdf.format(new Date());
        return utcTime;
    }
    private IARDrone drone;
    private CCPropertyManager props;
    private VisionVideoCanvas video;
    private VisionController visionController = null;
    private VisionTag[] tags;
    private JXMapKit jXMapKit;
    private GeoPosition currentPos;
    private IPAddressBasedGeoAcquirer geolocation;
    private double latitude;
    private double longitude;
    private TileFactoryInfo info;
    private DefaultTileFactory tileFactory;
    private TileFactoryInfo osmInfo;
    private TileFactoryInfo veInfo;
    WaypointPainter<Waypoint> waypointPainter;
    private final List<TileFactory> factories = new ArrayList<>();
    JComboBox tilesSelectorComboBox;
    private GPXParser p = new GPXParser();
    private ArrayList<org.alternativevision.gpx.beans.Waypoint> trackPoints
            = new ArrayList<>();
    private org.alternativevision.gpx.beans.Waypoint wayPoint;
    private Track track;
    private GPX gpx;
    private HashSet<Track> tracks;

    private JButton currentPositionButton;
    private JButton loadGPXButton;
    private JButton loadPathChooserButton;
    private JTextField loadPathLocation;
    private JButton saveGPXButton;
    private JButton savePathChooserButton;
    private JTextField savePathLocation;
    private JButton gpsButton;
    private JLabel gpsStateLabel;
    private JComboBox videoFormatComboBox;
    private JButton controllerButton;
    private JButton videoButton;
    private JButton recordButton;

    private GPSListener gpsListener = new GPSListener() {

        @Override
        public void posUpdated(double latitude, double longitude,
                double elevation) {
            wayPoint = new org.alternativevision.gpx.beans.Waypoint();
            wayPoint.setLatitude(latitude);
            wayPoint.setLongitude(longitude);
            wayPoint.setElevation(elevation);
            wayPoint.setTime(GetUTCdatetimeAsDate());
            trackPoints.add(wayPoint);
        }

        @Override
        public boolean isGPSactive(long gps_state) {
            if (gps_state != 0) {
                gpsStateLabel.setIcon(greenIcon);
            } else {
                gpsStateLabel.setIcon(redIcon);
            }
            return gps_state != 0;
        }
    };

    private IExceptionListener exceptionListener = new IExceptionListener() {
        @Override
        public void exeptionOccurred(ARDroneException exc) {
            if (exc instanceof ConfigurationException) {
                drone.start();
            } else if (exc instanceof CommandException) {
                drone.freeze();
                drone.start();
            } else if (exc instanceof NavDataException) {
                drone.freeze();
                drone.start();
            } else if (exc instanceof VideoException) {
                VideoManager vm = drone.getVideoManager();
                if (vm != null) {
                    vm.close();
                }
                drone.start();
            }
        }
    };

    public RunningApp() {
        super(new GridBagLayout());
        props = CCPropertyManager.getInstance();
    }

    public void init() throws InterruptedException {
        //------------------------ General interface ---------------------------
        JPanel generalOptionPanel = new JPanel(new GridBagLayout());
        //------------------------ Controller button  --------------------------
        controllerButton = new JButton("Enable Vision Controller");
        controllerButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (controllerButton.getText().equals("Enable Vision Controller")) {
                    controllerButton.setText("Disable Vision Controller");
                    enableAutoControl();
                } else {
                    controllerButton.setText("Enable Vision Controller");
                    disableAutoControl();
                }
            }
        });
        //------------------------ GPS button  ---------------------------------
        gpsButton = new JButton("Enable GPS");
        gpsButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (gpsButton.getText().equals("Enable GPS")) {
                    gpsButton.setText("Disable GPS");
                    enableGPS();
                    saveGPXButton.setEnabled(false);
                } else {
                    gpsButton.setText("Enable GPS");
                    disableGPS();
                    saveGPXButton.setEnabled(true);
                }
            }
        });
        //------------------------ GPS state label -----------------------------
        greenIcon = new ImageIcon(this.getClass().getResource("dot_green.png"));
        redIcon = new ImageIcon(this.getClass().getResource("dot_red.png"));
        gpsStateLabel = new JLabel("GPS state", redIcon, SwingConstants.LEFT);
        //------------------------ Record video button -------------------------
        recordButton = new JButton("Start Recording");
        recordButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (recordButton.getText().equals("Start Recording")) {
                    startRecording();
                    recordButton.setText("Stop Recording");
                } else {
                    stopRecording();
                    recordButton.setText("Start Recording");
                }
            }
        });
        //------------------------ Save to GPX button --------------------------
        saveGPXButton = new JButton("Save to GPX");
        saveGPXButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (trackPoints.isEmpty()) {
                    System.out.println("Veuillez activer le gps");
                } else {
                    try {
                        saveGPX();
                    } catch (ParserConfigurationException | TransformerException | IOException ex) {
                        Logger.getLogger(RunningApp.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        });
        //------------------------ Save path chooser button --------------------
        savePathChooserButton = new JButton("...");
        savePathChooserButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JFileChooser fc = new JFileChooser(props.getGPXStoragePath());
                fc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);

                int returnVal = fc.showOpenDialog(RunningApp.this);
                if (returnVal == JFileChooser.APPROVE_OPTION) {
                    String path = fc.getSelectedFile().getPath();
                    props.setGPXStoragePath(path);
                    savePathLocation.setText(path);
                }
            }
        });
        savePathLocation = new JTextField(props.getGPXStoragePath());
        generalOptionPanel.setBorder(BorderFactory.createTitledBorder("General Options"));
        generalOptionPanel.add(controllerButton, new GridBagConstraints(0, 0, 1, 1, 0, 0, GridBagConstraints.FIRST_LINE_START, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
        generalOptionPanel.add(recordButton, new GridBagConstraints(1, 0, 1, 1, 0, 0, GridBagConstraints.FIRST_LINE_START, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
        generalOptionPanel.add(gpsButton, new GridBagConstraints(2, 0, 1, 1, 0, 0, GridBagConstraints.FIRST_LINE_START, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
        generalOptionPanel.add(gpsStateLabel, new GridBagConstraints(3, 0, 1, 1, 0, 0, GridBagConstraints.FIRST_LINE_START, GridBagConstraints.NONE, new Insets(7, 0, 0, 0), 0, 0));
        generalOptionPanel.add(saveGPXButton, new GridBagConstraints(0, 1, 1, 1, 0, 0, GridBagConstraints.FIRST_LINE_START, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
        generalOptionPanel.add(savePathLocation, new GridBagConstraints(1, 1, 1, 1, 1, 0, GridBagConstraints.FIRST_LINE_START, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 0), 0, 0));
        generalOptionPanel.add(savePathChooserButton, new GridBagConstraints(2, 1, 1, 1, 0, 0, GridBagConstraints.FIRST_LINE_START, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
        //------------------------ Map Tab panels ------------------------------
        JPanel mapViewerPanel = new JPanel(new GridBagLayout());
        jXMapKit = new JXMapKit();
        //------------------------ JXMapKit panel interface --------------------
        geolocation = new IPAddressBasedGeoAcquirer();
        Position position = geolocation.acquireLocation(true);
        if (position != null) {
            Coordinates coords = position.getCoords();
            latitude = coords.getLatitude();
            longitude = coords.getLongitude();
            currentPos = new GeoPosition(latitude, longitude);
        } else {
            currentPos = new GeoPosition(0.0, 0.0);
        }
        info = new OSMTileFactoryInfo();
        tileFactory = new DefaultTileFactory(info);
        osmInfo = new OSMTileFactoryInfo();
        veInfo = new VirtualEarthTileFactoryInfo(VirtualEarthTileFactoryInfo.MAP);
        factories.add(new DefaultTileFactory(osmInfo));
        factories.add(new DefaultTileFactory(veInfo));
        jXMapKit.setTileFactory(factories.get(0));
        jXMapKit.setMiniMapVisible(false);
        jXMapKit.setZoom(7);
        jXMapKit.setAddressLocation(currentPos);
        jXMapKit.setAddressLocationShown(false);
        jXMapKit.setCenterPosition(currentPos);
        jXMapKit.getMainMap().addMouseMotionListener(new MouseMotionListener() {
            @Override
            public void mouseDragged(MouseEvent e) {

            }

            @Override
            public void mouseMoved(MouseEvent e) {
                JXMapViewer map = jXMapKit.getMainMap();
                Point2D worldPos = map.getTileFactory().geoToPixel(currentPos, map.getZoom());
                Rectangle rect = map.getViewportBounds();
                int sx = (int) worldPos.getX() - rect.x;
                int sy = (int) worldPos.getY() - rect.y;
                Point screenPos = new Point(sx, sy);
            }
        });
        String[] tfLabels = new String[factories.size()];
        for (int i = 0; i < factories.size(); i++) {
            tfLabels[i] = factories.get(i).getInfo().getName();
        }
        //------------------------ Map panel interface -------------------------
        JPanel mapPanel = new JPanel(new GridBagLayout());
        //------------------------ Tile selector combox ------------------------
        tilesSelectorComboBox = new JComboBox(tfLabels);
        tilesSelectorComboBox.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                TileFactory factory = factories.get(tilesSelectorComboBox.getSelectedIndex());
                jXMapKit.setTileFactory(factory);
            }
        });
        //------------------------ Current position button ---------------------
        currentPositionButton = new JButton("Go to current location");
        currentPositionButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                jXMapKit.setCenterPosition(currentPos);
            }
        });
        //------------------------ Load from GPX button ------------------------
        loadGPXButton = new JButton("load from GPX");
        loadGPXButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    loadGPX();
                } catch (ParserConfigurationException | SAXException | IOException ex) {
                    Logger.getLogger(RunningApp.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
        //------------------------ Load path chooser button --------------------
        loadPathChooserButton = new JButton("...");
        loadPathChooserButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                JFileChooser fc = new JFileChooser(props.getGPXStoragePath());
                fc.setFileSelectionMode(JFileChooser.FILES_ONLY);

                int returnVal = fc.showOpenDialog(RunningApp.this);
                if (returnVal == JFileChooser.APPROVE_OPTION) {
                    String path = fc.getSelectedFile().getName();
                    props.setGPXLoadFile(path);
                    loadPathLocation.setText(path);
                }
            }
        });
        loadPathLocation = new JTextField(props.getGPXLoadFile());
        //------------------------ Construction of the map panel ---------------
        Dimension dim = new Dimension(640, 120);
        mapPanel.setMinimumSize(dim);
        mapPanel.setMaximumSize(dim);
        mapPanel.setSize(dim);
        mapPanel.setBorder(BorderFactory.createTitledBorder("Map Options"));
        mapPanel.add(tilesSelectorComboBox, new GridBagConstraints(0, 0, 1, 1,
                0, 0, GridBagConstraints.FIRST_LINE_START,
                GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
        mapPanel.add(currentPositionButton, new GridBagConstraints(1, 0, 1, 1,
                0, 0, GridBagConstraints.FIRST_LINE_START,
                GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
        mapPanel.add(loadGPXButton, new GridBagConstraints(0, 1, 1, 1, 0, 0,
                GridBagConstraints.FIRST_LINE_START, GridBagConstraints.NONE,
                new Insets(0, 0, 0, 0), 0, 0));
        mapPanel.add(loadPathLocation, new GridBagConstraints(1, 1, 1, 1, 1, 1,
                GridBagConstraints.FIRST_LINE_START,
                GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 0), 0, 0));
        mapPanel.add(loadPathChooserButton, new GridBagConstraints(2, 1, 1, 1,
                0, 0, GridBagConstraints.FIRST_LINE_START,
                GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
        //------------------------ Construction of the map vierwer panel -------
        mapViewerPanel.add(jXMapKit, new GridBagConstraints(0, 0, 1, 1, 1, 1,
                GridBagConstraints.FIRST_LINE_START, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 0), 0, 0));
        mapViewerPanel.add(mapPanel, new GridBagConstraints(0, 1, 1, 1, 0, 0,
                GridBagConstraints.FIRST_LINE_START,
                GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 0), 0, 0));
        //------------------------ Video tab panels ----------------------------
        JPanel VideoPanel = new JPanel(new GridBagLayout());
        video = new VisionVideoCanvas(drone);
        JPanel videoOptionsPanel = new JPanel(new GridBagLayout());
        //------------------------ Video Options panel interface ---------------
        //------------------------ Video format combox -------------------------
        videoFormatComboBox = new JComboBox(new String[]{FORMAT_MP4, FORMAT_H264});
        videoFormatComboBox.setSelectedItem(props.getVideoFormat());
        videoFormatComboBox.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                props.setVideoFormat(videoFormatComboBox.getSelectedItem() + "");

                if (videoFormatComboBox.getSelectedItem().equals(FORMAT_MP4)) {
                    drone.getCommandManager().setVideoCodec(VideoCodec.MP4_360P);
                } else if (videoFormatComboBox.getSelectedItem().equals(FORMAT_H264)) {
                    drone.getCommandManager().setVideoCodec(VideoCodec.H264_720P);
                }

                drone.getVideoManager().reinitialize();
            }
        });
        if (props.getVideoFormat().equals(FORMAT_MP4)) {
            drone.getCommandManager().setVideoCodec(VideoCodec.MP4_360P);
        } else if (props.getVideoFormat().equals(FORMAT_H264)) {
            drone.getCommandManager().setVideoCodec(VideoCodec.H264_720P);
        }
        //------------------------ Video button --------------------------------
        videoButton = new JButton("Activate Video");
        videoButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (videoButton.getText().equals("Activate Video")) {
                    videoButton.setText("Desactivate Video");
                    enableVideo();
                } else {
                    videoButton.setText("Activate Video");
                    disableVideo();
                }
            }
        });
        //------------------------ Construction of the video option panel ------
        videoOptionsPanel.setBorder(BorderFactory.createTitledBorder("Video Options"));
        videoOptionsPanel.setMinimumSize(dim);
        videoOptionsPanel.setMaximumSize(dim);
        videoOptionsPanel.setSize(dim);
        videoOptionsPanel.add(videoButton, new GridBagConstraints(0, 0, 1, 1, 0, 0, GridBagConstraints.FIRST_LINE_START, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
        videoOptionsPanel.add(videoFormatComboBox, new GridBagConstraints(1, 0, 1, 1, 0, 0, GridBagConstraints.FIRST_LINE_START, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
        //------------------------ Construction of the video panel -------------
        VideoPanel.add(video, new GridBagConstraints(0, 0, 1, 1, 1, 1, GridBagConstraints.FIRST_LINE_START, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        VideoPanel.add(videoOptionsPanel, new GridBagConstraints(0, 1, 1, 1, 0, 0, GridBagConstraints.FIRST_LINE_START, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 0), 0, 0));
        //------------------------ Panel with tabs -----------------------------
        JTabbedPane tabPanel = new JTabbedPane();
        tabPanel.setTabPlacement(JTabbedPane.BOTTOM);
        add(tabPanel, new GridBagConstraints(0, 0, 1, 1, 1, 1, GridBagConstraints.FIRST_LINE_START, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        tabPanel.addTab("Video", VideoPanel);
        tabPanel.addTab("Map Viewer", mapViewerPanel);
        add(generalOptionPanel, new GridBagConstraints(0, 2, 1, 1, 0, 0, GridBagConstraints.FIRST_LINE_START, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 0), 0, 0));
    }

    public void enableVideo() {
        drone.getVideoManager().addImageListener(this);
    }

    public void disableVideo() {
        drone.getVideoManager().removeImageListener(this);
    }

    public void startRecording() {
        drone.getCommandManager().setVideoOnUsb(true);
        drone.getCommandManager().setVideoCodec(VideoCodec.MP4_360P_H264_720P);
    }

    public void stopRecording() {
        if (videoFormatComboBox.getSelectedItem().equals(FORMAT_MP4)) {
            drone.getCommandManager().setVideoCodec(VideoCodec.MP4_360P);
        } else if (videoFormatComboBox.getSelectedItem().equals(FORMAT_H264)) {
            drone.getCommandManager().setVideoCodec(VideoCodec.H264_720P);
        }
        drone.getCommandManager().setVideoOnUsb(false);
    }

    public void enableAutoControl() {
        drone.getCommandManager().setLedsAnimation(LEDAnimation.BLINK_STANDARD, 2.0f, 1);
        drone.getCommandManager().setEnemyColors(EnemyColor.ORANGE_BLUE);
        VisionTagType[] type = {VisionTagType.SHELL_TAG_V2};
        //drone.getCommandManager().setEnemyColors(EnemyColor.ORANGE_GREEN);
        //VisionTagType[] type = {VisionTagType.CAP};
        drone.getCommandManager().setDetectionType(DetectionType.HORIZONTAL, type);
        drone.getCommandManager().setDetectEnemyWithoutShell(false);
        visionController = new VisionController(drone);
        drone.getNavDataManager().addVisionListener(visionController);
        drone.getNavDataManager().addAttitudeListener(visionController);
        drone.getNavDataManager().addAltitudeListener(visionController);
        //visionController.start();
        System.out.println("Vision Controller enabled");
    }

    public void disableAutoControl() {
        drone.getCommandManager().setLedsAnimation(LEDAnimation.BLINK_STANDARD, 2.0f, 1);
        //visionController.stopController();
        drone.getNavDataManager().removeVisionListener(visionController);
        drone.getNavDataManager().removeAttitudeListener(visionController);
        drone.getNavDataManager().removeAltitudeListener(visionController);
        System.out.println("Vision Controller disabled");
    }

    public void enableGPS() {
        drone.getCommandManager().setLedsAnimation(LEDAnimation.BLINK_GREEN, 2.0f, 1);
        drone.getNavDataManager().addGPSListener(gpsListener);
    }

    public void disableGPS() {
        drone.getCommandManager().setLedsAnimation(LEDAnimation.BLINK_RED, 2.0f, 1);
        drone.getNavDataManager().removeGPSListener(gpsListener);
        gpsStateLabel.setIcon(redIcon);
    }

    public void saveGPX() throws FileNotFoundException, ParserConfigurationException, TransformerException {
        Date d = new Date();
        final String date = GPXDATEFORMAT.format(d);
        if (gpsButton.getText().equals("Enable GPS")) {
            track = new Track();
            track.setTrackPoints(trackPoints);
            gpx = new GPX();
            gpx.addTrack(track);
            gpx.setVersion(GPXVERSION);
            gpx.setCreator(GPXCREATOR);
            String fileName = savePathLocation.getText() + File.separatorChar + "ARDroneFollower_gpx_" + date + ".gpx";
            FileOutputStream out = new FileOutputStream(fileName);
            p.writeGPX(gpx, out);
        }
    }

    public void loadGPX() throws FileNotFoundException, ParserConfigurationException, SAXException, IOException {
        gpx = new GPX();
        String fileName = savePathLocation.getText() + File.separatorChar + loadPathLocation.getText();
        FileInputStream in = new FileInputStream(fileName);
        gpx = p.parseGPX(in);
        tracks = new HashSet<>();
        tracks = gpx.getTracks();
        for (Track trackloaded : tracks) {
            ArrayList<org.alternativevision.gpx.beans.Waypoint> waypointsloaded;
            waypointsloaded = trackloaded.getTrackPoints();
            List<GeoPosition> geoPositions = new ArrayList<>();
            for (org.alternativevision.gpx.beans.Waypoint waypointloaded : waypointsloaded) {
                geoPositions.add(new GeoPosition(waypointloaded.getLatitude(), waypointloaded.getLongitude()));
            }
            jXMapKit.getMainMap().zoomToBestFit(new HashSet<>(geoPositions), 0.7);
            ARdrone.app.controlcenter.plugins.mapviewer.RoutePainter routePainter = new ARdrone.app.controlcenter.plugins.mapviewer.RoutePainter(geoPositions);
            jXMapKit.getMainMap().setOverlayPainter(routePainter);
        }
    }

    @Override
    public void activate(IARDrone drone) {
        this.drone = drone;
        try {
            init();
        } catch (InterruptedException ex) {
            Logger.getLogger(RunningApp.class.getName()).log(Level.SEVERE, null, ex);
        }
        drone.addExceptionListener(exceptionListener);
    }

    @Override
    public void deactivate() {
        drone.removeExceptionListener(exceptionListener);
    }

    @Override
    public String getTitle() {
        return "Running App";
    }

    @Override
    public String getDescription() {
        return "Display the Running App";
    }

    @Override
    public boolean isVisual() {
        return true;
    }

    @Override
    public Dimension getScreenSize() {
        return new Dimension(650, 600);
    }

    @Override
    public Point getScreenLocation() {
        return new Point(655, 300);
    }

    @Override
    public JPanel getPanel() {
        return this;
    }

    @Override
    public void imageUpdated(BufferedImage image) {
        BufferedImage newImage = new BufferedImage(video.getWidth(), video.getHeight(), BufferedImage.TYPE_INT_RGB);
        Graphics g = newImage.createGraphics();
        g.drawImage(image, 0, 0, video.getWidth(), video.getHeight(), null);
        if (visionController != null) {
            if (visionController.tags != null) {
                g.setColor(Color.black);
                int x = (int) (visionController.tags[0].getX() * video.getWidth() / 1000.0);
                int y = (int) (visionController.tags[0].getY() * video.getHeight() / 1000.0);
                int w = (int) (visionController.tags[0].getWidth() * video.getWidth() / 1000.0);
                int h = (int) (visionController.tags[0].getHeight() * video.getHeight() / 1000.0);
                g.drawRect(x - w / 2, y - h / 2, w, h);
                g.fillOval(x, y, 5, 5);
            }
        }
        image = newImage;
        g.dispose();
        video.imageUpdated(image);
    }

}
