package ARdrone.app.controlcenter.plugins.connection;


import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Point;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import ARdrone.app.controlcenter.ICCPlugin;
import ARdrone.base.IARDrone;
import ARdrone.base.exception.ARDroneException;
import ARdrone.base.exception.CommandException;
import ARdrone.base.exception.ConfigurationException;
import ARdrone.base.exception.IExceptionListener;
import ARdrone.base.exception.NavDataException;
import ARdrone.base.exception.VideoException;

public class ConnectionState extends JPanel implements ICCPlugin
{
	private IARDrone drone;
	
	private static Icon greenIcon;
	private static Icon redIcon;
	
	private JLabel commandLabel;
	private JLabel configurationLabel;
	private JLabel videoLabel;
	private JLabel navdataLabel;
	
	private IExceptionListener exceptionListener;
	
	public ConnectionState()
	{
		super(new GridBagLayout());
		
		greenIcon = new ImageIcon(this.getClass().getResource("dot_green.png"));
		redIcon = new ImageIcon(this.getClass().getResource("dot_red.png"));
		
		commandLabel = new JLabel("Command Channel", greenIcon, SwingConstants.LEFT);
		configurationLabel = new JLabel("Configuration Channel", greenIcon, SwingConstants.LEFT);
		navdataLabel = new JLabel("Navdata Channel", greenIcon, SwingConstants.LEFT);
		videoLabel = new JLabel("Video Channel", greenIcon, SwingConstants.LEFT);
		
		exceptionListener = new IExceptionListener() {
                        @Override
			public void exeptionOccurred(ARDroneException exc)
			{
				if (exc instanceof ConfigurationException)
				{
					configurationLabel.setIcon(redIcon);
					configurationLabel.setToolTipText(exc+"");
				}
				else if (exc instanceof CommandException)
				{
					commandLabel.setIcon(redIcon);
					commandLabel.setToolTipText(exc+"");
				}
				else if (exc instanceof NavDataException)
				{
					navdataLabel.setIcon(redIcon);
					navdataLabel.setToolTipText(exc+"");
				}
				else if (exc instanceof VideoException)
				{
					videoLabel.setIcon(redIcon);
					videoLabel.setToolTipText(exc+"");
				}
			}
		};
		
		add(commandLabel, new GridBagConstraints(0, 0, 1, 1, 1, 1, GridBagConstraints.FIRST_LINE_START, GridBagConstraints.HORIZONTAL, new Insets(0,0,0,0), 0, 0));
		add(navdataLabel, new GridBagConstraints(0, 1, 1, 1, 1, 1, GridBagConstraints.FIRST_LINE_START, GridBagConstraints.HORIZONTAL, new Insets(0,0,0,0), 0, 0));
		add(configurationLabel, new GridBagConstraints(0, 2, 1, 1, 1, 1, GridBagConstraints.FIRST_LINE_START, GridBagConstraints.HORIZONTAL, new Insets(0,0,0,0), 0, 0));
		add(videoLabel, new GridBagConstraints(0, 3, 1, 1, 1, 1, GridBagConstraints.FIRST_LINE_START, GridBagConstraints.HORIZONTAL, new Insets(0,0,0,0), 0, 0));
	}
		
        @Override
	public void activate(IARDrone drone)
	{
		this.drone = drone;
		drone.addExceptionListener(exceptionListener);
	}
	
        @Override
	public void deactivate()
	{
		drone.removeExceptionListener(exceptionListener);
	}

        @Override
	public String getTitle()
	{
		return "Connection State";
	}
	
        @Override
	public String getDescription()
	{
		return "Shows the status of the current connections to the drone.";
	}

        @Override
	public boolean isVisual()
	{
		return true;
	}

        @Override
	public Dimension getScreenSize()
	{
		return new Dimension(150, 100);
	}
	
        @Override
	public Point getScreenLocation()
	{
		return new Point(0, 330);
	}
	
        @Override
	public JPanel getPanel()
	{
		return this;
	}
}
