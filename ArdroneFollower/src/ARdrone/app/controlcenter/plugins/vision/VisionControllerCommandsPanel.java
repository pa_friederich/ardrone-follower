/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ARdrone.app.controlcenter.plugins.vision;

import ARdrone.base.IARDrone;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.image.BufferedImage;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

/**
 *
 * @author Friederich Pierre-André
 */
public class VisionControllerCommandsPanel extends JPanel {

    private IARDrone drone;
    BufferedImage Image;
    private VisionController visionController = null;

    /**
     *
     * @param visionController
     */
    public VisionControllerCommandsPanel(VisionController visionController) {
        this.visionController = visionController;
        Dimension dim = new Dimension(640, 360);
        setMinimumSize(dim);
        setMaximumSize(dim);
        setSize(dim);
        init();
        //new Timer(15, paintTimer).start();
        new Thread(new Runnable() {

            @Override
            public void run() {
                while (true) {
                    SwingUtilities.invokeLater(new Runnable() {
                        @Override
                        public void run() {
                            repaint();
                        }
                    });
                    try {
                        Thread.sleep(15);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }

        }).start();
    }

    private void init() {
        Image = new BufferedImage(1280, 720, BufferedImage.TYPE_INT_RGB);
        Graphics g = Image.createGraphics();
        g.setColor(Color.black);
        g.fillRect(0, 0, Image.getWidth(), Image.getHeight());
        g.drawImage(Image, 0, 0, this);
    }
    
    @Override
    public void paint(Graphics g) {
        super.paint(g);
        if (Image != null) {
            if (visionController.tags != null) {
                g.drawImage(Image, 0, 0, getWidth(), getHeight(), null);
                g.setColor(Color.WHITE);
                int x = (int) (visionController.tags[0].getX() * getWidth() / 1000.0);
                int y = (int) (visionController.tags[0].getY() * getHeight() / 1000.0);
                int w = (int) (visionController.tags[0].getWidth() * getWidth() / 1000.0);
                int h = (int) (visionController.tags[0].getHeight() * getHeight() / 1000.0);
                g.drawRect(x - w / 2, y - h / 2, w, h);
                g.fillOval(x, y, 5, 5);
                Font fonte = new Font("Arial", Font.BOLD, 15);
                g.setFont(fonte);
//                g.drawString("x : " + visionController.camera.getX(), 30, 30);
//                g.drawString("y : " + visionController.camera.getY(), 30, 40);
//                g.drawString("z : " + visionController.camera.getZ(), 30, 50);
                g.drawString("x : " + visionController.P_o.get(0, 0), 30, 30);
                g.drawString("y : " + visionController.P_o.get(1, 0), 30, 40);
                g.drawString("z : " + visionController.P_o.get(2, 0), 30, 50);
                g.drawString("yaw : " + visionController.yaw, 30, 60);
                g.setColor(Color.RED);
                int imgCenterX = getWidth()/2;
                int imgCenterY = getHeight()/2;
                int tolerance = 140;
                g.drawRect(imgCenterX - tolerance / 2, imgCenterY - tolerance / 2, tolerance, tolerance);
                g.setColor(Color.YELLOW);
                g.drawRect((getWidth()/4)-100, imgCenterY-15, 100, 30);
                g.drawString("L", getWidth()/4-50, imgCenterY-20);
                g.drawRect(3*(getWidth()/4), imgCenterY-15, 100, 30);
                g.drawString("R", 3*(getWidth()/4)+50, imgCenterY-20);
                g.drawRect(imgCenterX-15, (getHeight()/4)-100, 30, 100);
                g.drawString("U", imgCenterX+30, (getHeight()/4)-50);
                g.drawRect(imgCenterX-15, 3*(getHeight()/4), 30, 100);
                g.drawString("D", imgCenterX+20, 3*(getHeight()/4)+50);
                g.drawRect(10, imgCenterY-100, 30, 200);
                g.drawString("F", 20, imgCenterY-110);
                g.drawString("B", 20, imgCenterY+120);
                g.drawLine(10, imgCenterY, 40, imgCenterY);
                if(visionController.cyaw < 0){
                    g.setColor(Color.YELLOW);
                    g.fillRect(3*(getWidth()/4), imgCenterY-15, -(int)(visionController.cyaw*100), 30);
                    g.setColor(Color.RED);
                    g.drawString(-(int)(visionController.cyaw*100) + " %", 3*(getWidth()/4)+50, imgCenterY+5);
                    g.drawString("0 %", getWidth()/4-50, imgCenterY+5);
                }
                else{
                    g.setColor(Color.YELLOW);
                    g.fillRect((getWidth()/4)-(int)(visionController.cyaw*100), imgCenterY-15, (int)(visionController.cyaw*100), 30);
                    g.setColor(Color.RED);
                    g.drawString("0 %", 3*(getWidth()/4)+50, imgCenterY+5);
                    g.drawString((int)(visionController.cyaw*100) +" %", getWidth()/4-50, imgCenterY+5);
                }
                if(visionController.cz < 0){
                    g.setColor(Color.YELLOW);
                    g.fillRect(imgCenterX-15, 3*(getHeight()/4), 30, -(int)(visionController.cz*100));
                    g.setColor(Color.RED);
                    g.drawString(-(int)(visionController.cz*100) + " %", imgCenterX-10, 3*(getHeight()/4)+50);
                    g.drawString("0 %", imgCenterX-10, getHeight()/4-50);
                }
                else{
                    g.setColor(Color.YELLOW);
                    g.fillRect(imgCenterX-15, (getHeight()/4)-(int)(visionController.cz*100), 30, (int)(visionController.cz*100));
                    g.setColor(Color.RED);
                    g.drawString("0 %", imgCenterX-10, 3*(getHeight()/4)+50);
                    g.drawString((int)(visionController.cz*100) +" %", imgCenterX-10, getHeight()/4-50);
                }
                if(visionController.cx > 0){
                    g.setColor(Color.YELLOW);
                    g.fillRect(10, imgCenterY, 30, (int)(visionController.cx*100));
                    g.setColor(Color.BLACK);
                    g.drawLine(10, imgCenterY, 40, imgCenterY);
                    g.setColor(Color.RED);
                    g.drawString((int)(visionController.cx*100) + " %", 12, imgCenterY+50);
                    g.drawString("0 %", 12, imgCenterY-50);
                }
                else{
                    g.setColor(Color.YELLOW);
                    g.fillRect(10, imgCenterY+(int)(visionController.cx*100), 30, -(int)(visionController.cx*100));
                    g.setColor(Color.BLACK);
                    g.drawLine(10, imgCenterY, 40, imgCenterY);
                    g.setColor(Color.RED);
                    g.drawString("0 %", 12, imgCenterY+50);
                    g.drawString(-(int)(visionController.cx*100) +" %", 12, imgCenterY-50);
                }
                g.dispose();
                
            } else {
                g.drawImage(Image, 0, 0, getWidth(), getHeight(), null);
                Font fonte = new Font("Arial", Font.BOLD, 15);
                g.setFont(fonte);
                g.setColor(Color.RED);
                int imgCenterX = getWidth()/2;
                int imgCenterY = getHeight()/2;
                g.drawString("No Tag", 30, 30);
                g.setColor(Color.YELLOW);
                g.drawRect((getWidth()/4)-100, imgCenterY-15, 100, 30);
                g.drawString("L", getWidth()/4-50, imgCenterY-20);
                g.drawRect(3*(getWidth()/4), imgCenterY-15, 100, 30);
                g.drawString("R", 3*(getWidth()/4)+50, imgCenterY-20);
                g.drawRect(imgCenterX-15, (getHeight()/4)-100, 30, 100);
                g.drawString("U", imgCenterX+20, (getHeight()/4)-50);
                g.drawRect(imgCenterX-15, 3*(getHeight()/4), 30, 100);
                g.drawString("D", imgCenterX+20, 3*(getHeight()/4)+50);
                g.drawRect(10, imgCenterY-100, 30, 200);
                g.drawString("F", 20, imgCenterY-110);
                g.drawString("B", 20, imgCenterY+120);
                g.drawLine(10, imgCenterY, 40, imgCenterY);
                g.dispose();
            }
        } else {
            g.setColor(Color.WHITE);
            g.fillRect(0, 0, getWidth(), getHeight());
        }
    }

    Action paintTimer = new AbstractAction() {
        @Override
        public void actionPerformed(ActionEvent e) {
            repaint();
            }
        };
}