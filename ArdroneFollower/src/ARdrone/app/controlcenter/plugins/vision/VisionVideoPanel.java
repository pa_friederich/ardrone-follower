package ARdrone.app.controlcenter.plugins.vision;

import ARdrone.app.controlcenter.CCPropertyManager;
import ARdrone.app.controlcenter.ICCPlugin;
import ARdrone.base.IARDrone;
import ARdrone.base.command.DetectionType;
import ARdrone.base.command.EnemyColor;
import ARdrone.base.command.LEDAnimation;
import ARdrone.base.command.VideoCodec;
import ARdrone.base.command.VisionTagType;
import ARdrone.base.navdata.VisionTag;
import ARdrone.base.video.ImageListener;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JPanel;

/**
 *
 * @author pa_friederich
 */
public class VisionVideoPanel extends JPanel implements ICCPlugin, ImageListener {

    /**
     *
     */
    public final static String FORMAT_MP4 = "MPEG-4";

    /**
     *
     */
    public final static String FORMAT_H264 = "H.264";

    private VisionVideoCanvas video;
    private IARDrone drone;
    private VisionController visionController = null;

    private JComboBox videoFormatComboBox;
    private JButton controllerButton;

    private CCPropertyManager props;

    private int imageWidth;
    private int imageHeight;
    private boolean doScaleImage;
    VisionTag[] tags;

    /**
     *
     */
    public VisionVideoPanel() {
        super(new GridBagLayout());
        props = CCPropertyManager.getInstance();
        doScaleImage = props.isScaleVideo();
    }

    /**
     *
     * @param drone
     */
    @Override
    public void activate(IARDrone drone) {
        this.drone = drone;
        init();
        drone.getVideoManager().addImageListener(this);
    }

    private void init() {
        video = new VisionVideoCanvas(drone);

        // Video options -------------------------------------------------------
        videoFormatComboBox = new JComboBox(new String[]{FORMAT_MP4, FORMAT_H264});
        videoFormatComboBox.setSelectedItem(props.getVideoFormat());
        videoFormatComboBox.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                props.setVideoFormat(videoFormatComboBox.getSelectedItem() + "");

                if (videoFormatComboBox.getSelectedItem().equals(FORMAT_MP4)) {
                    drone.getCommandManager().setVideoCodec(VideoCodec.MP4_360P);
                } else if (videoFormatComboBox.getSelectedItem().equals(FORMAT_H264)) {
                    drone.getCommandManager().setVideoCodec(VideoCodec.H264_720P);
                }

                drone.getVideoManager().reinitialize();
            }
        });

        if (props.getVideoFormat().equals(FORMAT_MP4)) {
            drone.getCommandManager().setVideoCodec(VideoCodec.MP4_360P);
        } else if (props.getVideoFormat().equals(FORMAT_H264)) {
            drone.getCommandManager().setVideoCodec(VideoCodec.H264_720P);
        }

        final JCheckBox scaleImage = new JCheckBox("Scale image to fit window", props.isScaleVideo());
        ActionListener scaleListener = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
                doScaleImage = scaleImage.isSelected();
                props.setScaleVideo(scaleImage.isSelected());
            }
        };
        scaleImage.addActionListener(scaleListener);

        JPanel videoOptionsPanel = new JPanel(new GridBagLayout());
        videoOptionsPanel.setBorder(BorderFactory.createTitledBorder("Video Options"));
        videoOptionsPanel.add(scaleImage, new GridBagConstraints(0, 0, 1, 1, 1, 0, GridBagConstraints.FIRST_LINE_START, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 0), 0, 0));
        videoOptionsPanel.add(videoFormatComboBox, new GridBagConstraints(2, 0, 1, 1, 0, 0, GridBagConstraints.FIRST_LINE_START, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));

        // Controller options --------------------------------------------------
        controllerButton = new JButton("Enable Vision Controller");
        controllerButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (controllerButton.getText().equals("Enable Vision Controller")) {
                    controllerButton.setText("Disable Vision Controller");
                    enableAutoControl();
                } else {
                    controllerButton.setText("Enable Vision Controller");
                    disableAutoControl();
                }
            }
        });

        JPanel controllerOptionsPanel = new JPanel(new GridBagLayout());
        controllerOptionsPanel.setBorder(BorderFactory.createTitledBorder("Controller Options"));
        controllerOptionsPanel.add(controllerButton, new GridBagConstraints(0, 0, 1, 1, 0, 0, GridBagConstraints.FIRST_LINE_START, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));

        // Options Panel -------------------------------------------------------
        JPanel options = new JPanel(new GridBagLayout());
        options.add(videoOptionsPanel, new GridBagConstraints(0, 0, 1, 1, 0, 0, GridBagConstraints.FIRST_LINE_START, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 0), 0, 0));
        options.add(controllerOptionsPanel, new GridBagConstraints(0, 1, 1, 1, 0, 0, GridBagConstraints.FIRST_LINE_START, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 0), 0, 0));

        Dimension dim = new Dimension(640, 120);
        options.setMinimumSize(dim);
        options.setMaximumSize(dim);
        options.setSize(dim);

        add(video, new GridBagConstraints(0, 0, 1, 1, 1, 1, GridBagConstraints.FIRST_LINE_START, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        add(options, new GridBagConstraints(0, 1, 1, 1, 1, 0, GridBagConstraints.FIRST_LINE_START, GridBagConstraints.HORIZONTAL, new Insets(5, 0, 0, 0), 0, 0));

    }

    /**
     *
     */
    @Override
    public void deactivate() {
        drone.getVideoManager().removeImageListener(this);
    }

    /**
     *
     * @return
     */
    @Override
    public String getTitle() {
        return "Vision controller";
    }

    /**
     *
     * @return
     */
    @Override
    public String getDescription() {
        return "Display the vision controller";
    }

    /**
     *
     * @return
     */
    @Override
    public boolean isVisual() {
        return true;
    }

    /**
     *
     * @return
     */
    @Override
    public Dimension getScreenSize() {
        return new Dimension(650, 550);
    }

    /**
     *
     * @return
     */
    @Override
    public Point getScreenLocation() {
        return new Point(0, 300);
    }

    /**
     *
     * @return
     */
    @Override
    public JPanel getPanel() {
        return this;
    }

    /**
     *
     * @param image
     */
    @Override
    public void imageUpdated(BufferedImage image) {
        if (doScaleImage) {
            BufferedImage newImage = new BufferedImage(video.getWidth(), video.getHeight(), BufferedImage.TYPE_INT_RGB);

            Graphics g = newImage.createGraphics();
            g.drawImage(image, 0, 0, video.getWidth(), video.getHeight(), null);
            g.dispose();

            image = newImage;
        }
        if (visionController != null) {
            if (visionController.tags != null) {
                Graphics g = image.getGraphics();
                g.setColor(Color.black);
                int x = (int) (visionController.tags[0].getX() * video.getWidth() / 1000.0);
                int y = (int) (visionController.tags[0].getY() * video.getHeight() / 1000.0);
                int w = (int) (visionController.tags[0].getWidth() * video.getWidth() / 1000.0);
                int h = (int) (visionController.tags[0].getHeight() * video.getHeight() / 1000.0);
                g.drawRect(x - w / 2, y - h / 2, w, h);
                g.fillOval(x, y, 5, 5);
                Font fonte = new Font(" TimesRoman ", Font.BOLD, 10);
                g.setFont(fonte);
                g.drawString("x : " + visionController.camera.getX() + " y : " + visionController.camera.getY() + " z : " + visionController.camera.getZ(), 30, 30);
                g.dispose();
            }
        }
        video.imageUpdated(image);
    }

    /**
     *
     */
    public void enableAutoControl() {
        drone.getCommandManager().setLedsAnimation(LEDAnimation.BLINK_STANDARD, 2.0f, 1);
        drone.getCommandManager().setEnemyColors(EnemyColor.ORANGE_BLUE);
        VisionTagType[] type = {VisionTagType.SHELL_TAG_V2};
        //drone.getCommandManager().setEnemyColors(EnemyColor.ORANGE_GREEN);
        //VisionTagType[] type = {VisionTagType.CAP};
        drone.getCommandManager().setDetectionType(DetectionType.HORIZONTAL, type);
        drone.getCommandManager().setDetectEnemyWithoutShell(false);
        visionController = new VisionController(drone);
        drone.getNavDataManager().addVisionListener(visionController);
        drone.getNavDataManager().addAttitudeListener(visionController);
        drone.getNavDataManager().addAltitudeListener(visionController);
        //visionController.start();
        System.out.println("Vision Controller enabled");
    }

    /**
     *
     */
    public void disableAutoControl() {
        drone.getCommandManager().setLedsAnimation(LEDAnimation.BLINK_STANDARD, 2.0f, 1);
        visionController.stopController();
        drone.getNavDataManager().removeVisionListener(visionController);
        drone.getNavDataManager().removeAttitudeListener(visionController);
        drone.getNavDataManager().removeAltitudeListener(visionController);
        System.out.println("Vision Controller disabled");
    }
}
