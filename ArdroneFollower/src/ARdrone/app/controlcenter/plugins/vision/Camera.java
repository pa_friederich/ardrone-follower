package ARdrone.app.controlcenter.plugins.vision;

import Jama.*;

/**
 *
 * @author Friederich Pierre-André
 */

public class Camera {
    
    protected Matrix I_c  = new Matrix(3,3);
    public Matrix P    = new Matrix(3,1);
    protected Matrix p    = new Matrix(3,1);
    
    /**
     *
     * @param xPixel
     * @param yPixel
     * @param distance
     */
    public void pixelToCentimeters (int xPixel, int yPixel, int distance){
        final double xRatio = 1280.0/1000.0;
        final double yRatio = 720.0/1000.0;
        this.p.set(0, 0, xPixel*xRatio);
        this.p.set(1, 0, yPixel*yRatio);
        this.p.set(2, 0, 1);
        
        //Camera intrinsic parameters matrix
        // first line
        this.I_c.set(0, 0, 1115.6);
        this.I_c.set(0, 1, 0);
        this.I_c.set(0, 2, 614.9);
        // second line
        this.I_c.set(1, 0, 0);
        this.I_c.set(1, 1, 1112.6);
        this.I_c.set(1, 2, 420.1);
        //third line
        this.I_c.set(2, 0, 0);
        this.I_c.set(2, 1, 0);
        this.I_c.set(2, 2, 1);
        
        // p in camera space -> P
        P = I_c.inverse().times(this.p).times(distance);
    }

    /**
     *
     * @return
     */
        public double getX(){
        return this.P.get(0, 0);
    }
    
    /**
     *
     * @return
     */
    public double getY(){
        return this.P.get(1, 0);
    }
    
    /**
     *
     * @return
     */
    public double getZ(){
        return this.P.get(2, 0);
    }
}
