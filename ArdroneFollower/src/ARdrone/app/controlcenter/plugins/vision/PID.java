/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ARdrone.app.controlcenter.plugins.vision;

/**
 *
 * @author pa_friederich
 */
public class PID {
    
    private double Kp;
    private double Ki;
    private double Kd;
    
    private double lastTime;
    private double lastError;
    static private double errorSum;
    
    /**
     *
     * @param Kp
     * @param Ki
     * @param Kd
     */
    public PID(double Kp, double Ki, double Kd){
        this.Kp = Kp;
        this.Ki = Ki;
        this.Kd = Kd;
    }
    
    /**
     *
     * @param Kp
     * @param Ki
     * @param Kd
     */
    public void setPID(double Kp, double Ki, double Kd){
        this.Kp = Kp;
        this.Ki = Ki;
        this.Kd = Kd;
    }
    
    /**
     *
     */
    public void resetPID(){
        this.lastTime = 0;
        this.lastError = Double.POSITIVE_INFINITY;
        PID.errorSum = 0;
    }
    
    /**
     *
     * @param e
     * @return
     */
    public double PIDgetCommand(double e)
    {
        double time = System.currentTimeMillis();
        double dt = time - this.lastTime;
        double de = 0;
        
        if(this.lastTime != 0){
            if(this.lastError < Double.POSITIVE_INFINITY){
                de = (e - this.lastError)/dt;
            }
        PID.errorSum += e*dt; 
        }
        
        this.lastTime = time;
        this.lastError = e;
        
        double command = this.Kp*e + this.Ki*PID.errorSum + this.Kd*de;
        
        return command;
    }
    
    /**
     *
     * @return
     */
    public double getP(){
        return this.Kp;
    }
    
    /**
     *
     * @return
     */
    public double getI(){
        return this.Ki;
    }
    
    /**
     *
     * @return
     */
    public double getD(){
        return this.Kd;
    }
    
}