/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ARdrone.app.controlcenter.plugins.vision;

import ARdrone.base.navdata.Altitude;
import Jama.*;

import ARdrone.base.navdata.AttitudeListener;
import ARdrone.base.navdata.AltitudeListener;
import ARdrone.base.navdata.TrackerData;
import ARdrone.base.navdata.VelocityListener;
import ARdrone.base.navdata.VisionData;
import ARdrone.base.navdata.VisionListener;
import ARdrone.base.navdata.VisionPerformance;
import ARdrone.base.navdata.VisionTag;

/**
 *
 * @author pa_friederich
 */
public class ExtendedKalmanFilter {

    protected class EKFState {

        float x;
        float y;
        float z;
        float distance;
        float yaw;
        float pitch;
        float roll;
    };
    
    protected class Odometry {
        
        float dx;
        float dy;
        float dyaw;
    }
    
    float deltaT = 1/15;

    EKFState state = new EKFState();
    Matrix sigma = new Matrix(3, 3);
    Matrix Q = new Matrix(3, 3);
    Matrix R = new Matrix(3, 3);
    float lastYaw = 0;

    public EKFState getEKFState() {
        return this.state;
    }

    /**
     *
     * @return
     */
    public Matrix getConfidence() {
        return this.sigma;
    }

    public void EKFReset() {
        this.sigma = Matrix.identity(3, 3);
        double q[][] = {{0.0003, 0, 0}, {0, 0.0003, 0}, {0, 0, 0.0001}};
        this.Q = new Matrix(q);
        double r[][] = {{0.3, 0, 0}, {0, 0.3, 0}, {0, 0, 0.3}};
        this.R = new Matrix(r);
        this.lastYaw = 0;
    }
    
    private AttitudeListener attitudeListener = new AttitudeListener() {

        @Override
        public void attitudeUpdated(float pitch, float roll, float yaw) {
            
        }

        @Override
        public void attitudeUpdated(float pitch, float roll) {
            
        }

        @Override
        public void windCompensation(float pitch, float roll) {
            
        }
    };
    
    private AltitudeListener altitudeListener = new AltitudeListener() {

        @Override
        public void receivedAltitude(int altitude) {
            
        }

        @Override
        public void receivedExtendedAltitude(Altitude d) {
            
        }
    };
    
    private VelocityListener velocityListener = new VelocityListener() {
        
        @Override
        public void velocityChanged(float vx, float vy, float vz) {
        }
        
    };
    
    private VisionListener visionListener = new VisionListener() {

        @Override
        public void tagsDetected(VisionTag[] tags) {
            
        }

        @Override
        public void trackersSend(TrackerData trackersData) {
            
        }

        @Override
        public void receivedPerformanceData(VisionPerformance d) {
            
        }

        @Override
        public void receivedRawData(float[] vision_raw) {
            
        }

        @Override
        public void receivedData(VisionData d) {
            
        }

        @Override
        public void receivedVisionOf(float[] of_dx, float[] of_dy) {
            
        }

        @Override
        public void typeDetected(int detection_camera_type) {
            
        }
    };
    
    public void EKFPredict() {
        
        float pitch = 0;
        float roll = 0;
        float yaw = 0;
        VisionTag [] tags = null;
        int altitude = 0;
        float vx = 0;
        float vy = 0;
        float vz = 0;
        float dt = this.deltaT;
        attitudeListener.attitudeUpdated(pitch, roll, yaw);
        altitudeListener.receivedAltitude(altitude);
        velocityListener.velocityChanged(vx/1000, vy/1000, vz/1000);
        visionListener.tagsDetected(tags);
        
        
       if(this.lastYaw == 0){
           this.lastYaw = yaw;
       }
       
       Odometry odometry = new Odometry();
       odometry.dx = vx*dt;
       odometry.dy = vy*dt;
       odometry.dyaw = yaw - this.lastYaw;
       
       this.lastYaw = yaw;
    }
}
