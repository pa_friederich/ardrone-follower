/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ARdrone.app.controlcenter.plugins.vision;

import ARdrone.app.controlcenter.CCPropertyManager;
import ARdrone.app.controlcenter.ICCPlugin;
import ARdrone.base.IARDrone;
import ARdrone.base.command.DetectionType;
import ARdrone.base.command.EnemyColor;
import ARdrone.base.command.LEDAnimation;
import ARdrone.base.command.VisionTagType;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 *
 * @author Friederich Pierre-André
 */
public class DebugToolsPanel extends JPanel implements ICCPlugin {
    
    private IARDrone drone;
    private CCPropertyManager props;
    
    private VisionController visionController = null;
    private VisionControllerCommandsPanel debug;
    
    private JButton controllerButton;
    private JButton setPIDXButton;
    private JButton setPIDYButton;
    private JButton setPIDZButton;
    private JButton setPIDYawButton;
    
    /**
     *
     */
    public DebugToolsPanel(){
        super(new GridBagLayout());
        props = CCPropertyManager.getInstance();
    }
    
    /**
     *
     */
    public void init(){
        debug = new VisionControllerCommandsPanel(visionController);
        
        // ControllerPanel -----------------------------------------------------
        
        controllerButton = new JButton("Enable Vision Controller");
        controllerButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (controllerButton.getText().equals("Enable Vision Controller")) {
                    controllerButton.setText("Disable Vision Controller");
                    enableAutoControl();
                } else {
                    controllerButton.setText("Enable Vision Controller");
                    disableAutoControl();
                }
            }
        });
        
        // PID Settings --------------------------------------------------------
        
        // Panel for the forward-backward PID controller
        
        JPanel PIDXPanel = new JPanel(new GridBagLayout());
        PIDXPanel.setPreferredSize(new Dimension(360, 60));
        PIDXPanel.setBorder(BorderFactory.createTitledBorder("X PID"));
        JLabel PXLabel = new JLabel("P : ");
        JLabel IXLabel = new JLabel("I : ");
        JLabel DXLabel = new JLabel("D : ");
        String Px = String.valueOf(visionController.PIDX.getP());
        String Ix = String.valueOf(visionController.PIDX.getI());
        String Dx = String.valueOf(visionController.PIDX.getD());
        final JTextField PX = new JTextField(Px);
        final JTextField IX = new JTextField(Ix);
        final JTextField DX = new JTextField(Dx);
        PX.setPreferredSize(new Dimension(60, 25));
        IX.setPreferredSize(new Dimension(60, 25));
        DX.setPreferredSize(new Dimension(60, 25));

        setPIDXButton = new JButton("Set Value");
        setPIDXButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                visionController.PIDX.setPID(Double.parseDouble(PX.getText()), Double.parseDouble(IX.getText()), Double.parseDouble(DX.getText()));
                visionController.PIDX.resetPID();
            }
        });
        
        PIDXPanel.add(PXLabel, new GridBagConstraints(0, 0, 1, 1, 0, 0, GridBagConstraints.FIRST_LINE_START, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        PIDXPanel.add(PX, new GridBagConstraints(1, 0, 1, 1, 0, 0, GridBagConstraints.FIRST_LINE_START, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        PIDXPanel.add(IXLabel, new GridBagConstraints(2, 0, 1, 1, 0, 0, GridBagConstraints.FIRST_LINE_START, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        PIDXPanel.add(IX, new GridBagConstraints(3, 0, 1, 1, 0, 0, GridBagConstraints.FIRST_LINE_START, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        PIDXPanel.add(DXLabel, new GridBagConstraints(4, 0, 1, 1, 0, 0, GridBagConstraints.FIRST_LINE_START, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        PIDXPanel.add(DX, new GridBagConstraints(5, 0, 1, 1, 0, 0, GridBagConstraints.FIRST_LINE_START, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        PIDXPanel.add(setPIDXButton, new GridBagConstraints(6, 0, 1, 1, 0, 0, GridBagConstraints.FIRST_LINE_START, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        
        
        // Panel for the left-right PID controller
        
        JPanel PIDYPanel = new JPanel(new GridBagLayout());
        PIDYPanel.setPreferredSize(new Dimension(360, 60));
        PIDYPanel.setBorder(BorderFactory.createTitledBorder("Y PID"));
        JLabel PYLabel = new JLabel("P : ");
        JLabel IYLabel = new JLabel("I : ");
        JLabel DYLabel = new JLabel("D : ");
        String Py = String.valueOf(visionController.PIDYAW.getP());
        String Iy = String.valueOf(visionController.PIDYAW.getI());
        String Dy = String.valueOf(visionController.PIDYAW.getD());
        final JTextField PY = new JTextField(Py);
        final JTextField IY = new JTextField(Iy);
        final JTextField DY = new JTextField(Dy);
        PY.setPreferredSize(new Dimension(60, 25));
        IY.setPreferredSize(new Dimension(60, 25));
        DY.setPreferredSize(new Dimension(60, 25));
        
        setPIDYButton = new JButton("Set Value");
        setPIDYButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                visionController.PIDYAW.setPID(Double.parseDouble(PY.getText()), Double.parseDouble(IY.getText()), Double.parseDouble(DY.getText()));
                visionController.PIDYAW.resetPID();
            }
        });
        
        PIDYPanel.add(PYLabel, new GridBagConstraints(0, 0, 1, 1, 0, 0, GridBagConstraints.FIRST_LINE_START, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        PIDYPanel.add(PY, new GridBagConstraints(1, 0, 1, 1, 0, 0, GridBagConstraints.FIRST_LINE_START, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        PIDYPanel.add(IYLabel, new GridBagConstraints(2, 0, 1, 1, 0, 0, GridBagConstraints.FIRST_LINE_START, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        PIDYPanel.add(IY, new GridBagConstraints(3, 0, 1, 1, 0, 0, GridBagConstraints.FIRST_LINE_START, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        PIDYPanel.add(DYLabel, new GridBagConstraints(4, 0, 1, 1, 0, 0, GridBagConstraints.FIRST_LINE_START, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        PIDYPanel.add(DY, new GridBagConstraints(5, 0, 1, 1, 0, 0, GridBagConstraints.FIRST_LINE_START, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        PIDYPanel.add(setPIDYButton, new GridBagConstraints(6, 0, 1, 1, 0, 0, GridBagConstraints.FIRST_LINE_START, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        
        // Panel for the up-down PID controller
        
        JPanel PIDZPanel = new JPanel(new GridBagLayout());
        PIDZPanel.setPreferredSize(new Dimension(360, 60));
        PIDZPanel.setBorder(BorderFactory.createTitledBorder("Z PID"));
        JLabel PZLabel = new JLabel("P : ");
        JLabel IZLabel = new JLabel("I : ");
        JLabel DZLabel = new JLabel("D : ");
        String Pz = String.valueOf(visionController.PIDZ.getP());
        String Iz = String.valueOf(visionController.PIDZ.getI());
        String Dz = String.valueOf(visionController.PIDZ.getD());
        final JTextField PZ = new JTextField(Pz);
        final JTextField IZ = new JTextField(Iz);
        final JTextField DZ = new JTextField(Dz);
        PZ.setPreferredSize(new Dimension(60, 25));
        IZ.setPreferredSize(new Dimension(60, 25));
        DZ.setPreferredSize(new Dimension(60, 25));
        
        setPIDZButton = new JButton("Set Value");
        setPIDZButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                visionController.PIDZ.setPID(Double.parseDouble(PZ.getText()), Double.parseDouble(IZ.getText()), Double.parseDouble(DZ.getText()));
                visionController.PIDZ.resetPID();
            }
        });
        
        PIDZPanel.add(PZLabel, new GridBagConstraints(0, 0, 1, 1, 0, 0, GridBagConstraints.FIRST_LINE_START, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        PIDZPanel.add(PZ, new GridBagConstraints(1, 0, 1, 1, 0, 0, GridBagConstraints.FIRST_LINE_START, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        PIDZPanel.add(IZLabel, new GridBagConstraints(2, 0, 1, 1, 0, 0, GridBagConstraints.FIRST_LINE_START, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        PIDZPanel.add(IZ, new GridBagConstraints(3, 0, 1, 1, 0, 0, GridBagConstraints.FIRST_LINE_START, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        PIDZPanel.add(DZLabel, new GridBagConstraints(4, 0, 1, 1, 0, 0, GridBagConstraints.FIRST_LINE_START, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        PIDZPanel.add(DZ, new GridBagConstraints(5, 0, 1, 1, 0, 0, GridBagConstraints.FIRST_LINE_START, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        PIDZPanel.add(setPIDZButton, new GridBagConstraints(6, 0, 1, 1, 0, 0, GridBagConstraints.FIRST_LINE_START, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        
        // Panel for the angular orientation PID controller
        
        JPanel PIDYawPanel = new JPanel(new GridBagLayout());
        PIDYawPanel.setPreferredSize(new Dimension(360, 60));
        PIDYawPanel.setBorder(BorderFactory.createTitledBorder("Yaw PID"));
        JLabel PYawLabel = new JLabel("P : ");
        JLabel IYawLabel = new JLabel("I : ");
        JLabel DYawLabel = new JLabel("D : ");
        String Pyaw = String.valueOf(visionController.PIDYAW.getP());
        String Iyaw = String.valueOf(visionController.PIDYAW.getI());
        String Dyaw = String.valueOf(visionController.PIDYAW.getD());
        final JTextField PYaw = new JTextField(Pyaw);
        final JTextField IYaw = new JTextField(Iyaw);
        final JTextField DYaw = new JTextField(Dyaw);
        PYaw.setPreferredSize(new Dimension(60, 25));
        IYaw.setPreferredSize(new Dimension(60, 25));
        DYaw.setPreferredSize(new Dimension(60, 25));
        
        setPIDYawButton = new JButton("Set Value");
        setPIDYawButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                visionController.PIDYAW.setPID(Double.parseDouble(PYaw.getText()), Double.parseDouble(IYaw.getText()), Double.parseDouble(DYaw.getText()));
                visionController.PIDYAW.resetPID();
            }
        });
        
        PIDYawPanel.add(PYawLabel, new GridBagConstraints(0, 0, 1, 1, 0, 0, GridBagConstraints.FIRST_LINE_START, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        PIDYawPanel.add(PYaw, new GridBagConstraints(1, 0, 1, 1, 0, 0, GridBagConstraints.FIRST_LINE_START, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        PIDYawPanel.add(IYawLabel, new GridBagConstraints(2, 0, 1, 1, 0, 0, GridBagConstraints.FIRST_LINE_START, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        PIDYawPanel.add(IYaw, new GridBagConstraints(3, 0, 1, 1, 0, 0, GridBagConstraints.FIRST_LINE_START, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        PIDYawPanel.add(DYawLabel, new GridBagConstraints(4, 0, 1, 1, 0, 0, GridBagConstraints.FIRST_LINE_START, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        PIDYawPanel.add(DYaw, new GridBagConstraints(5, 0, 1, 1, 0, 0, GridBagConstraints.FIRST_LINE_START, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        PIDYawPanel.add(setPIDYawButton, new GridBagConstraints(6, 0, 1, 1, 0, 0, GridBagConstraints.FIRST_LINE_START, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        
        JPanel PIDsettingsPanel = new JPanel(new GridBagLayout());
        PIDsettingsPanel.setBorder(BorderFactory.createTitledBorder("PID settings"));
        PIDsettingsPanel.add(PIDXPanel, new GridBagConstraints(0, 0, 1, 1, 0, 0, GridBagConstraints.FIRST_LINE_START, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
        PIDsettingsPanel.add(PIDZPanel, new GridBagConstraints(0, 1, 1, 1, 0, 0, GridBagConstraints.FIRST_LINE_START, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
        PIDsettingsPanel.add(PIDYawPanel, new GridBagConstraints(0, 2, 1, 1, 0, 0, GridBagConstraints.FIRST_LINE_START, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
        
        JPanel visionControllerPanel = new JPanel(new GridBagLayout());
        visionControllerPanel.setBorder(BorderFactory.createTitledBorder("Vision Controller"));
        visionControllerPanel.add(controllerButton, new GridBagConstraints(0, 0, 1, 1, 0, 0, GridBagConstraints.FIRST_LINE_START, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        
        JPanel options = new JPanel(new GridBagLayout());
        options.add(PIDsettingsPanel, new GridBagConstraints(0, 0, 1, 1, 1, 0, GridBagConstraints.FIRST_LINE_START, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
        options.add(visionControllerPanel, new GridBagConstraints(0, 1, 1, 1, 0, 0, GridBagConstraints.FIRST_LINE_START, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));

        
        Dimension dim = new Dimension(380, 60);
        options.setMinimumSize(dim);
        options.setMaximumSize(dim);
        options.setSize(dim);

        add(debug, new GridBagConstraints(0, 0, 1, 1, 1, 1, GridBagConstraints.FIRST_LINE_START, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        add(options, new GridBagConstraints(1, 0, 1, 1, 0, 0, GridBagConstraints.FIRST_LINE_START, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));


    };
    
    /**
     *
     */
    public void enableAutoControl(){
        drone.getCommandManager().setLedsAnimation(LEDAnimation.SNAKE_GREEN_RED, 2.0f, 1);
        drone.getCommandManager().setEnemyColors(EnemyColor.ORANGE_BLUE);
        VisionTagType[] type = {VisionTagType.SHELL_TAG_V2};
        //drone.getCommandManager().setEnemyColors(EnemyColor.ORANGE_GREEN);
        //VisionTagType[] type = {VisionTagType.CAP};
        drone.getCommandManager().setDetectionType(DetectionType.HORIZONTAL, type);
        drone.getCommandManager().setDetectEnemyWithoutShell(false);
        //visionController = new VisionController(drone);
        drone.getNavDataManager().addVisionListener(visionController);
        drone.getNavDataManager().addAttitudeListener(visionController);
        //visionController.start();
    }
    
    /**
     *
     */
    public void disableAutoControl(){
        drone.getCommandManager().setLedsAnimation(LEDAnimation.RED_SNAKE, 2.0f, 1);
        visionController.stopController();
        drone.getNavDataManager().removeVisionListener(visionController);
        drone.getNavDataManager().removeAttitudeListener(visionController);
    }

    /**
     *
     * @param drone
     */
    @Override
    public void activate(IARDrone drone) {
        this.drone = drone;
        visionController = new VisionController(drone);
        init();
    }

    /**
     *
     */
    @Override
    public void deactivate() {
    }

    /**
     *
     * @return
     */
    @Override
    public String getTitle() {
        return "DebuggingTools";
    }

    /**
     *
     * @return
     */
    @Override
    public String getDescription() {
        return "Display the degugging tools panel";
    }

    /**
     *
     * @return
     */
    @Override
    public boolean isVisual() {
        return true;
    }

    /**
     *
     * @return
     */
    @Override
    public Dimension getScreenSize() {
        return new Dimension(1000, 500);
    }

    /**
     *
     * @return
     */
    @Override
    public Point getScreenLocation() {
        return new Point(0, 300);
    }

    /**
     *
     * @return
     */
    @Override
    public JPanel getPanel() {
        return this;
    }
    
}
