package ARdrone.app.controlcenter.plugins.altitude;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Point;

import javax.swing.JPanel;

import org.jfree.chart.ChartPanel;

import ARdrone.app.controlcenter.ICCPlugin;
import ARdrone.app.controlcenter.plugins.yawchart.CSVWriter;
import ARdrone.app.controlcenter.plugins.yawchart.YawChartPanel;
import ARdrone.base.IARDrone;
import ARdrone.base.navdata.Altitude;
import ARdrone.base.navdata.AltitudeListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JButton;
import javax.swing.SwingUtilities;

public class AltitudeChartPanel extends JPanel implements ICCPlugin {

    private IARDrone drone;

    private AltitudeChart chart;

    private int altitudechart;

    private JButton Button;
    protected boolean doStop = true;
    Thread thread;
    
    private String csv = "altitude_output.csv";
    private CSVWriter writer;
    private List<String[]> data = new ArrayList<>();
    private double ms = 0;

    public AltitudeChartPanel() {
        super(new GridBagLayout());

        this.chart = new AltitudeChart();
        JPanel chartPanel = new ChartPanel(chart.getChart(), true, true, true, true, true);

        Button = new JButton("Start Recording");
        Button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (Button.getText().equals("Start Recording")) {
                    doStop = false;
                    thread = new Thread(new Runnable() {

                        @Override
                        public void run() {
                            while (!doStop) {
                                SwingUtilities.invokeLater(new Runnable() {
                                    @Override
                                    public void run() {
                                        chart.setAltitude(altitudechart);
                                        data.add(new String[]{Double.toString(ms), Double.toString(altitudechart)});
                                        ms +=15.0e-3;
                                    }
                                });
                                try {
                                    Thread.sleep(15);
                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                    });
                    thread.start();
                    Button.setText("Stop Recording");
                } else {
                    doStop = true;
                    Button.setText("Start Recording");
                    try {
                        SaveCSV();
                    } catch (IOException ex) {
                        Logger.getLogger(YawChartPanel.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    ms = 0;
                }
            }
        });

        add(chartPanel, new GridBagConstraints(0, 0, 1, 1, 1, 1, GridBagConstraints.FIRST_LINE_START, GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
        add(Button, new GridBagConstraints(0, 1, 1, 1, 0, 0, GridBagConstraints.FIRST_LINE_START, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 0), 0, 0));

    }

    private AltitudeListener altitudeListener = new AltitudeListener() {

        public void receivedAltitude(int altitude) {
            altitudechart = altitude;
        }

        public void receivedExtendedAltitude(Altitude altitude) {
        }

    };

    public void activate(IARDrone drone) {
        this.drone = drone;

        drone.getNavDataManager().addAltitudeListener(altitudeListener);
    }

    public void deactivate() {
        drone.getNavDataManager().removeAltitudeListener(altitudeListener);
    }

    public String getTitle() {
        return "Altitude Chart";
    }

    public String getDescription() {
        return "Displays a chart with the latest altitude";
    }

    public boolean isVisual() {
        return true;
    }

    public Dimension getScreenSize() {
        return new Dimension(330, 250);
    }

    public Point getScreenLocation() {
        return new Point(330, 390);
    }

    public JPanel getPanel() {
        return this;
    }
    
    public void SaveCSV() throws IOException{
        writer = new CSVWriter(new FileWriter(csv));
        writer.writeAll(data); 
        writer.close();
    }
}
