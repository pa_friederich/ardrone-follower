package ARdrone.app.controlcenter.plugins.console;

import ARdrone.app.controlcenter.ICCPlugin;
import ARdrone.base.IARDrone;
import java.awt.*;
import java.awt.event.*;
import java.io.*;
import javax.swing.*;

public class ConsolePanel extends JPanel implements ICCPlugin
{
	private JTextArea text;
	private JCheckBox checkBox;
	
	public ConsolePanel()
	{
		super(new GridBagLayout());
		
		checkBox = new JCheckBox("Redirect Console", false);
		checkBox.addActionListener(new ActionListener() {
                        @Override
			public void actionPerformed(ActionEvent e)
			{
				redirectSystemStreams(checkBox.isSelected());
			}
		});
		
		text = new JTextArea("Waiting for State ...\n");
//		text.setEditable(false);
		text.setFont(new Font("Courier", Font.PLAIN, 10));
                JScrollPane scrollPane = new JScrollPane(text);
                SmartScroller smartScroller = new SmartScroller(scrollPane, SmartScroller.END);
		
		add(scrollPane, new GridBagConstraints(0,0,1,1,1,1,GridBagConstraints.FIRST_LINE_START, GridBagConstraints.BOTH, new Insets(0,0,0,0), 0, 0));
		add(checkBox, new GridBagConstraints(0,1,1,1,1,0,GridBagConstraints.FIRST_LINE_START, GridBagConstraints.NONE, new Insets(0,0,0,0), 0, 0));
		
	}

	public void focus()
	{
		text.setFocusable(true);
		text.setRequestFocusEnabled(true);
		text.requestFocus();
	}
	
	private void redirectSystemStreams(boolean enableRedirect)
	{
		if (enableRedirect)
		{
			OutputStream out = new OutputStream() {
                                @Override
				public void write(int b) throws IOException
				{
					updateTextArea(String.valueOf((char) b));
				}
	
                                @Override
				public void write(byte[] b, int off, int len) throws IOException
				{
					updateTextArea(new String(b, off, len));
				}
	
                                @Override
				public void write(byte[] b) throws IOException
				{
					write(b, 0, b.length);
				}
			};
			System.setOut(new PrintStream(out, true));
			System.setErr(new PrintStream(out, true));
		}
		else
		{
			System.setOut(System.out);
			System.setErr(System.err);
		}
	}

	private void updateTextArea(final String text)
	{
		SwingUtilities.invokeLater(new Runnable() {
                        @Override
			public void run()
			{
				ConsolePanel.this.text.append(text);
			}
		});
	}

        @Override
	public void activate(IARDrone drone)
	{
		redirectSystemStreams(checkBox.isSelected());
	}

        @Override
	public void deactivate()
	{
		redirectSystemStreams(false);
	}
	
        @Override
	public String getTitle()
	{
		return "Logging Console";
	}
	
        @Override
	public String getDescription()
	{
		return "Redirects System.out/err and displays log information in an own panel.";
	}

        @Override
	public boolean isVisual()
	{
		return true;
	}

        @Override
	public Dimension getScreenSize()
	{
		return new Dimension(400, 300);
	}
	
        @Override
	public Point getScreenLocation()
	{
		return new Point(600, 400);
	}

        @Override
	public JPanel getPanel()
	{
		return this;
	}	
}
