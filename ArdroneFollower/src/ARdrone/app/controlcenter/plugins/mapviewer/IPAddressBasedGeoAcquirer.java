package ARdrone.app.controlcenter.plugins.mapviewer;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.StringTokenizer;

import ARdrone.base.geolocation.w3c.Coordinates;
import ARdrone.base.geolocation.w3c.Position;
import java.util.logging.Level;
import java.util.logging.Logger;

public class IPAddressBasedGeoAcquirer extends GeolocationAcquirer {

    private String getCurrentIPAddress() throws MalformedURLException, IOException {
        HttpURLConnection conn = (HttpURLConnection) new URL("http://ifconfig.me/ip").openConnection();
        BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
        String response = reader.readLine();
        if (conn.getResponseCode() == 200) {
            return response.trim();
        } else {
            return null;
        }
    }

    @Override
    protected Position acquireLocation(boolean enableHighAccuracy) throws InterruptedException {
        HttpURLConnection conn = null;
        String ipAddress = null;
        try {
            ipAddress = getCurrentIPAddress();
        } 
        catch (MalformedURLException e) {
            e.printStackTrace();
        } 
        catch (IOException e) {
            e.printStackTrace();
        }
        if (ipAddress != null) {
            try {
                conn = (HttpURLConnection) new URL("http://freegeoip.net/csv/" + ipAddress).openConnection();
                BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                String response = reader.readLine();
                if (conn.getResponseCode() == 200) {
                    StringTokenizer stTok = new StringTokenizer(response, ",");
                    stTok.nextToken(); // IP Address
                    stTok.nextToken(); // country code
                    stTok.nextToken(); // country
                    stTok.nextToken(); // state code
                    stTok.nextToken(); // state
                    stTok.nextToken(); // city
                    stTok.nextToken(); // zip code
                    String x = stTok.nextToken().substring(1);
                    x = x.substring(0, x.length() - 1);
                    double latitude = Double.parseDouble(x);
                    x = stTok.nextToken().substring(1);
                    x = x.substring(0, x.length() - 1);
                    double longitude = Double.parseDouble(x);

                    Coordinates coords = new Coordinates(latitude, longitude, new Double(0), 0, new Double(0), null, new Double(0));
                    Position pos = new Position(coords, -1);
                    conn.disconnect();
                    return pos;
                } else {
                    return null;
                }
            } catch (IOException ex) {
                Logger.getLogger(IPAddressBasedGeoAcquirer.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            return null;
        }

        return null;
    }

}
