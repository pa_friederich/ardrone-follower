/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ARdrone.app.controlcenter.plugins.mapviewer;

import ARdrone.app.controlcenter.CCPropertyManager;
import ARdrone.app.controlcenter.ICCPlugin;
import ARdrone.base.IARDrone;
import ARdrone.base.command.LEDAnimation;
import ARdrone.base.geolocation.w3c.Coordinates;
import ARdrone.base.geolocation.w3c.Position;
import ARdrone.base.navdata.GPSListener;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;
import java.awt.geom.Point2D;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.Border;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import org.alternativevision.gpx.GPXParser;
import org.alternativevision.gpx.beans.GPX;
import org.alternativevision.gpx.beans.Track;
import org.jxmapviewer.JXMapKit;
import org.jxmapviewer.JXMapViewer;
import org.jxmapviewer.OSMTileFactoryInfo;
import org.jxmapviewer.VirtualEarthTileFactoryInfo;
import org.jxmapviewer.viewer.DefaultTileFactory;
import org.jxmapviewer.viewer.GeoPosition;
import org.jxmapviewer.viewer.TileFactory;
import org.jxmapviewer.viewer.TileFactoryInfo;
import org.jxmapviewer.viewer.Waypoint;
import org.jxmapviewer.viewer.WaypointPainter;
import org.xml.sax.SAXException;

/**
 *
 * @author pa_friederich
 */
public class MapViewerPanel extends JPanel implements ICCPlugin {

    private static final String GPXCREATOR = "ARDroneFOllower";
    private static final String GPXVERSION = "1.1";
    private static final SimpleDateFormat GPXDATEFORMAT = new SimpleDateFormat("yyyy-MM-dd_HHmm", Locale.US);
    static final String DATEFORMAT = "yyyy-MM-dd HH:mm:ss";

    private static Icon greenIcon;
    private static Icon redIcon;

    public static Date StringDateToDate(String StrDate) {
        Date dateToReturn = null;
        SimpleDateFormat dateFormat = new SimpleDateFormat(DATEFORMAT);

        try {
            dateToReturn = (Date) dateFormat.parse(StrDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return dateToReturn;
    }

    public static Date GetUTCdatetimeAsDate() {
        //note: doesn't check for null
        return StringDateToDate(GetUTCdatetimeAsString());
    }

    public static String GetUTCdatetimeAsString() {
        final SimpleDateFormat sdf = new SimpleDateFormat(DATEFORMAT);
        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
        final String utcTime = sdf.format(new Date());

        return utcTime;
    }

    private JXMapKit jXMapKit;
    private GeoPosition currentPos;
    private IPAddressBasedGeoAcquirer geolocation;
    private double latitude;
    private double longitude;

    private IARDrone drone;
    private CCPropertyManager props;

    private TileFactoryInfo info;
    DefaultTileFactory tileFactory;
    TileFactoryInfo osmInfo;
    TileFactoryInfo veInfo;
    WaypointPainter<Waypoint> waypointPainter;
    private final List<TileFactory> factories = new ArrayList<>();

    JComboBox tilesSelectorComboBox;
    JButton CurrentPositionButton;
    private JButton GPSButton;
    private JButton saveGPXButton;
    private JButton savePathChooserButton;
    private JTextField savePathLocation;
    private JButton loadGPXButton;
    private JButton loadPathChooserButton;
    private JTextField loadPathLocation;
    private JLabel gpsStateLabel;

    GPXParser p = new GPXParser();
    private ArrayList<org.alternativevision.gpx.beans.Waypoint> trackPoints = new ArrayList<>();
    private org.alternativevision.gpx.beans.Waypoint wayPoint;
    private Track track;
    private GPX gpx;
    private HashSet<Track> tracks;

    private GPSListener gpsListener = new GPSListener() {

        @Override
        public void posUpdated(double latitude, double longitude, double elevation) {
            MapViewerPanel.this.currentPos = new GeoPosition(latitude, longitude);
            wayPoint = new org.alternativevision.gpx.beans.Waypoint();
            wayPoint.setLatitude(latitude);
            wayPoint.setLongitude(longitude);
            wayPoint.setElevation(elevation);
            wayPoint.setTime(GetUTCdatetimeAsDate());
            trackPoints.add(wayPoint);
        }

        @Override
        public boolean isGPSactive(long gps_state) {
            if (gps_state != 0) {
                gpsStateLabel.setIcon(greenIcon);
            } else {
                gpsStateLabel.setIcon(redIcon);
            }
            return gps_state != 0;
        }
    };

    public MapViewerPanel() {
        super(new GridBagLayout());
        props = CCPropertyManager.getInstance();
    }

    private void init() throws InterruptedException {

        jXMapKit = new JXMapKit();
        geolocation = new IPAddressBasedGeoAcquirer();
        Position position = geolocation.acquireLocation(true);
        if (position != null) {
            Coordinates coords = position.getCoords();
            latitude = coords.getLatitude();
            longitude = coords.getLongitude();
            currentPos = new GeoPosition(latitude, longitude);
        } else {
            currentPos = new GeoPosition(0.0, 0.0);
        }
        info = new OSMTileFactoryInfo();
        tileFactory = new DefaultTileFactory(info);

        osmInfo = new OSMTileFactoryInfo();
        veInfo = new VirtualEarthTileFactoryInfo(VirtualEarthTileFactoryInfo.MAP);
        //		factories.add(new EmptyTileFactory());
        factories.add(new DefaultTileFactory(osmInfo));
        factories.add(new DefaultTileFactory(veInfo));
        jXMapKit.setTileFactory(factories.get(0));
        jXMapKit.setMiniMapVisible(false);
        jXMapKit.setZoom(7);
        jXMapKit.setAddressLocation(currentPos);
        jXMapKit.setAddressLocationShown(false);
        jXMapKit.setCenterPosition(currentPos);
        jXMapKit.getMainMap().addMouseMotionListener(new MouseMotionListener() {
            @Override
            public void mouseDragged(MouseEvent e) {
                // ignore
            }

            @Override
            public void mouseMoved(MouseEvent e) {
                JXMapViewer map = jXMapKit.getMainMap();
                // convert to world bitmap
                Point2D worldPos = map.getTileFactory().geoToPixel(currentPos, map.getZoom());

                // convert to screen
                Rectangle rect = map.getViewportBounds();
                int sx = (int) worldPos.getX() - rect.x;
                int sy = (int) worldPos.getY() - rect.y;
                Point screenPos = new Point(sx, sy);
            }
        });

        String[] tfLabels = new String[factories.size()];
        for (int i = 0; i < factories.size(); i++) {
            tfLabels[i] = factories.get(i).getInfo().getName();
        }

        tilesSelectorComboBox = new JComboBox(tfLabels);
        tilesSelectorComboBox.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                TileFactory factory = factories.get(tilesSelectorComboBox.getSelectedIndex());
                jXMapKit.setTileFactory(factory);
            }
        });

        CurrentPositionButton = new JButton("Go to current location");
        CurrentPositionButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                jXMapKit.setCenterPosition(currentPos);
            }
        });

        // GPSButton -----------------------------------------------------------
        GPSButton = new JButton("Enable GPS");
        GPSButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (GPSButton.getText().equals("Enable GPS")) {
                    GPSButton.setText("Disable GPS");
                    enableGPS();
                    saveGPXButton.setEnabled(false);
                } else {
                    GPSButton.setText("Enable GPS");
                    disableGPS();
                    saveGPXButton.setEnabled(true);
                }
            }
        });
        // GPS State Label -----------------------------------------------------
        greenIcon = new ImageIcon(this.getClass().getResource("dot_green.png"));
        redIcon = new ImageIcon(this.getClass().getResource("dot_red.png"));

        gpsStateLabel = new JLabel("GPS state", redIcon, SwingConstants.LEFT);

        // saveGPXButton -------------------------------------------------------
        saveGPXButton = new JButton("Save to GPX");
        saveGPXButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (trackPoints.isEmpty()) {
                    System.out.println("Veuillez activer le gps");
                } else {
                    try {
                        saveGPX();
                    } catch (ParserConfigurationException | TransformerException | IOException ex) {
                        Logger.getLogger(MapViewerPanel.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        });

        savePathChooserButton = new JButton("...");
        savePathChooserButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JFileChooser fc = new JFileChooser(props.getGPXStoragePath());
                fc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);

                int returnVal = fc.showOpenDialog(MapViewerPanel.this);
                if (returnVal == JFileChooser.APPROVE_OPTION) {
                    String path = fc.getSelectedFile().getPath();
                    props.setGPXStoragePath(path);
                    savePathLocation.setText(path);
                }
            }
        });
        savePathLocation = new JTextField(props.getGPXStoragePath());

        // loadGPXButton -------------------------------------------------------
        loadGPXButton = new JButton("load from GPX");
        loadGPXButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    loadGPX();
                } catch (ParserConfigurationException | SAXException | IOException ex) {
                    Logger.getLogger(MapViewerPanel.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });

        loadPathChooserButton = new JButton("...");
        loadPathChooserButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                JFileChooser fc = new JFileChooser(props.getGPXStoragePath());
                fc.setFileSelectionMode(JFileChooser.FILES_ONLY);

                int returnVal = fc.showOpenDialog(MapViewerPanel.this);
                if (returnVal == JFileChooser.APPROVE_OPTION) {
                    String path = fc.getSelectedFile().getName();
                    props.setGPXLoadFile(path);
                    loadPathLocation.setText(path);
                }
            }
        });
        loadPathLocation = new JTextField(props.getGPXLoadFile());

        JPanel TilePanel = new JPanel(new GridBagLayout());
        Dimension dim = new Dimension(640, 120);
        TilePanel.setMinimumSize(dim);
        TilePanel.setMaximumSize(dim);
        TilePanel.setSize(dim);
        TilePanel.setBorder(BorderFactory.createTitledBorder("Map Options"));
        TilePanel.add(tilesSelectorComboBox, new GridBagConstraints(0, 0, 1, 1, 0, 0, GridBagConstraints.FIRST_LINE_START, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
        TilePanel.add(CurrentPositionButton, new GridBagConstraints(1, 0, 1, 1, 0, 0, GridBagConstraints.FIRST_LINE_START, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));

        JPanel GPSPanel = new JPanel(new GridBagLayout());
        GPSPanel.setMinimumSize(dim);
        GPSPanel.setMaximumSize(dim);
        GPSPanel.setSize(dim);
        GPSPanel.setBorder(BorderFactory.createTitledBorder("GPS Options"));
        GPSPanel.add(GPSButton, new GridBagConstraints(0, 0, 1, 1, 0, 0, GridBagConstraints.FIRST_LINE_START, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
        GPSPanel.add(gpsStateLabel, new GridBagConstraints(1, 0, 1, 1, 0, 0, GridBagConstraints.FIRST_LINE_START, GridBagConstraints.NONE, new Insets(7, 0, 0, 0), 0, 0));
        GPSPanel.add(saveGPXButton, new GridBagConstraints(0, 1, 1, 1, 0, 0, GridBagConstraints.FIRST_LINE_START, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
        GPSPanel.add(savePathLocation, new GridBagConstraints(1, 1, 1, 1, 1, 0, GridBagConstraints.FIRST_LINE_START, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 0), 0, 0));
        GPSPanel.add(savePathChooserButton, new GridBagConstraints(2, 1, 1, 1, 0, 0, GridBagConstraints.FIRST_LINE_START, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
        GPSPanel.add(loadGPXButton, new GridBagConstraints(0, 2, 1, 1, 0, 0, GridBagConstraints.FIRST_LINE_START, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
        GPSPanel.add(loadPathLocation, new GridBagConstraints(1, 2, 1, 1, 1, 1, GridBagConstraints.FIRST_LINE_START, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 0), 0, 0));
        GPSPanel.add(loadPathChooserButton, new GridBagConstraints(2, 2, 1, 1, 0, 0, GridBagConstraints.FIRST_LINE_START, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));

        add(jXMapKit, new GridBagConstraints(0, 0, 1, 1, 1, 1, GridBagConstraints.FIRST_LINE_START, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        add(TilePanel, new GridBagConstraints(0, 1, 1, 1, 0, 0, GridBagConstraints.FIRST_LINE_START, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 0), 0, 0));
        add(GPSPanel, new GridBagConstraints(0, 2, 1, 1, 0, 0, GridBagConstraints.FIRST_LINE_START, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 0), 0, 0));

    }

    public void enableGPS() {
        drone.getCommandManager().setLedsAnimation(LEDAnimation.BLINK_GREEN, 2.0f, 1);
        drone.getNavDataManager().addGPSListener(gpsListener);
    }

    public void disableGPS() {
        drone.getCommandManager().setLedsAnimation(LEDAnimation.BLINK_RED, 2.0f, 1);
        drone.getNavDataManager().removeGPSListener(gpsListener);
        gpsStateLabel.setIcon(redIcon);
    }

    public void saveGPX() throws FileNotFoundException, ParserConfigurationException, TransformerException {
        Date d = new Date();
        final String date = GPXDATEFORMAT.format(d);
        if (GPSButton.getText().equals("Enable GPS")) {
            track = new Track();
            track.setTrackPoints(trackPoints);
            gpx = new GPX();
            gpx.addTrack(track);
            gpx.setVersion(GPXVERSION);
            gpx.setCreator(GPXCREATOR);
            String fileName = savePathLocation.getText() + File.separatorChar + "ARDroneFollower_gpx_" + date + ".gpx";
            FileOutputStream out = new FileOutputStream(fileName);
            p.writeGPX(gpx, out);
        }
    }

    public void loadGPX() throws FileNotFoundException, ParserConfigurationException, SAXException, IOException {
        gpx = new GPX();
        String fileName = savePathLocation.getText() + File.separatorChar + loadPathLocation.getText();
        FileInputStream in = new FileInputStream(fileName);
        gpx = p.parseGPX(in);
        tracks = new HashSet<>();
        tracks = gpx.getTracks();
        for (Track trackloaded : tracks) {
            ArrayList<org.alternativevision.gpx.beans.Waypoint> waypointsloaded;
            waypointsloaded = trackloaded.getTrackPoints();
            List<GeoPosition> geoPositions = new ArrayList<>();
            for (org.alternativevision.gpx.beans.Waypoint waypointloaded : waypointsloaded) {
                geoPositions.add(new GeoPosition(waypointloaded.getLatitude(), waypointloaded.getLongitude()));
            }
            jXMapKit.getMainMap().zoomToBestFit(new HashSet<>(geoPositions), 0.7);
            RoutePainter routePainter = new RoutePainter(geoPositions);
            jXMapKit.getMainMap().setOverlayPainter(routePainter);
        }
    }

    @Override
    public void activate(IARDrone drone) {
        this.drone = drone;
        try {
            init();
        } catch (InterruptedException ex) {
            Logger.getLogger(MapViewerPanel.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void deactivate() {
    }

    @Override
    public String getTitle() {
        return "MapViewer";
    }

    @Override
    public String getDescription() {
        return "Display the map viewer";
    }

    @Override
    public boolean isVisual() {
        return true;
    }

    @Override
    public Dimension getScreenSize() {
        return new Dimension(650, 600);
    }

    @Override
    public Point getScreenLocation() {
        return new Point(655, 300);
    }

    @Override
    public JPanel getPanel() {
        return this;
    }

}
