package ARdrone.app.controlcenter.plugins.battery;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Point;

import javax.swing.JPanel;

import ARdrone.app.controlcenter.ICCPlugin;
import ARdrone.base.IARDrone;
import ARdrone.base.navdata.BatteryListener;

public class BatteryPanel extends JPanel implements ICCPlugin {

    private IARDrone drone;

    private final Font font = new Font("Helvetica", Font.PLAIN, 10);
    private int batteryLevel = 100;
    private int voltageLevel;

    public BatteryPanel() {
        setSize(20, 60);
    }

    @Override
    public void paint(Graphics g) {
        g.setColor(Color.black);
        g.drawRect(0, 0, this.getWidth(), this.getHeight());

        Color c;
        if (batteryLevel >= 50) {
            c = new Color((Math.abs(batteryLevel - 100f) / 100f) * 2f, 1f, 0f);
        } else {
            c = new Color(1f, (batteryLevel / 100f) * 2f, 0f);
        }

        g.setColor(c);
        g.fillRect(0, getHeight() * (batteryLevel / 100), this.getWidth(), this.getHeight());

        FontMetrics metrics = g.getFontMetrics(font);
        int hgt = metrics.getHeight();
        g.setFont(font);

        g.setColor(Color.black);
        g.drawString("Battery", (this.getWidth() / 2) - (metrics.stringWidth("Battery") / 2), (this.getHeight() / 2) - (hgt+5 / 2));
        g.drawString(batteryLevel + " %", (this.getWidth() / 2) - (metrics.stringWidth(batteryLevel + " %") / 2), (this.getHeight() / 2) + (hgt / 2));
        g.drawString(voltageLevel + " V", (this.getWidth() / 2) - (metrics.stringWidth(voltageLevel + " V") / 2), (int) ((this.getHeight() / 2) + ((hgt-5 / 2) * 2.5)));
    }

    private BatteryListener batteryListener = new BatteryListener() {

        @Override
        public void voltageChanged(int vbat_raw) {

        }

        @Override
        public void batteryLevelChanged(int batteryLevel) {
            if (batteryLevel != BatteryPanel.this.batteryLevel) {
                BatteryPanel.this.batteryLevel = batteryLevel;
                repaint();
            }
        }
    };

    @Override
    public void activate(IARDrone drone) {
        this.drone = drone;
        drone.getNavDataManager().addBatteryListener(batteryListener);
    }

    @Override
    public void deactivate() {
        drone.getNavDataManager().removeBatteryListener(batteryListener);
    }

    @Override
    public String getTitle() {
        return "Battery";
    }

    @Override
    public String getDescription() {
        return "Displays current battery and voltage levels";
    }

    @Override
    public boolean isVisual() {
        return true;
    }

    @Override
    public Dimension getScreenSize() {
        return new Dimension(95, 150);
    }

    @Override
    public Point getScreenLocation() {
        return new Point(870, 0);
    }

    @Override
    public JPanel getPanel() {
        return this;
    }
}
