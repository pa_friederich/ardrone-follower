package ARdrone.app.controlcenter.plugins.distancechart;

import ARdrone.app.controlcenter.plugins.yawchart.*;
import ARdrone.app.controlcenter.ICCPlugin;
import ARdrone.app.controlcenter.plugins.vision.Camera;
import ARdrone.base.IARDrone;
import ARdrone.base.navdata.AttitudeListener;
import ARdrone.base.navdata.TrackerData;
import ARdrone.base.navdata.VisionData;
import ARdrone.base.navdata.VisionListener;
import ARdrone.base.navdata.VisionPerformance;
import ARdrone.base.navdata.VisionTag;
import Jama.Matrix;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import org.jfree.chart.ChartPanel;

public class DistanceChartPanel extends JPanel implements ICCPlugin {

    private IARDrone drone;
    protected boolean doStop = true;
    protected int xc;
    protected int yc;
    protected int distance;
    protected double altitude;
    protected double rollchart;
    protected double pitchchart;
    protected double distanceChart;
    protected Matrix P_d = new Matrix(3, 1);
    protected Matrix P_o = new Matrix(3, 1);
    double[][] r_dc = {{0, 1, 0}, {0, 0, -1}, {-1, 0, 0}};
    protected Matrix R_dc = new Matrix(r_dc);
    double[][] r_do = {{Math.cos(pitchchart), Math.sin(rollchart) * Math.sin(pitchchart), Math.cos(rollchart) * Math.sin(pitchchart)},
    {0, Math.cos(rollchart), -Math.sin(rollchart)},
    {-Math.sin(pitchchart), Math.cos(pitchchart) * Math.sin(rollchart), Math.cos(rollchart) * Math.cos(pitchchart)}};
    protected Matrix R_do = new Matrix(r_do);
    Camera camera = new Camera();
    private DistanceChart chart;
    private JButton Button;
    Thread thread;
    boolean tag = false;
    
    private String csv = "distance_output.csv";
    private CSVWriter writer;
    private List<String[]> data = new ArrayList<>();
    private double ms = 0;

    public DistanceChartPanel() {
        super(new GridBagLayout());

        this.chart = new DistanceChart();
        JPanel chartPanel = new ChartPanel(chart.getChart(), true, true, true, true, true);

        Button = new JButton("Start Recording");
        Button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (Button.getText().equals("Start Recording")) {
                    doStop = false;
                    thread = new Thread(new Runnable() {

                        @Override
                        public void run() {
                            while (!doStop) {
                                SwingUtilities.invokeLater(new Runnable() {
                                    @Override
                                    public void run() {
                                        if(tag == false)
                                            distanceChart = 0;
                                        chart.setYaw((float) distanceChart);
                                        data.add(new String[]{Double.toString(ms), Double.toString(distanceChart)});
                                        ms +=15.0e-3;
                                        tag = false;
                                    }
                                });
                                try {
                                    Thread.sleep(15);
                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                    });
                    thread.start();
                    Button.setText("Stop Recording");
                } else {
                    doStop = true;
                    Button.setText("Start Recording");
                    try {
                        SaveCSV();
                    } catch (IOException ex) {
                        Logger.getLogger(DistanceChartPanel.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    ms = 0;
                }
            }
        });

        add(chartPanel, new GridBagConstraints(0, 0, 1, 1, 1, 1, GridBagConstraints.FIRST_LINE_START, GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
        add(Button, new GridBagConstraints(0, 1, 1, 1, 0, 0, GridBagConstraints.FIRST_LINE_START, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 0), 0, 0));

    }

    private VisionListener visionListener = new VisionListener() {

        @Override
        public void tagsDetected(VisionTag[] tags) {
            if (tags != null) {
                xc = tags[0].getX();
                yc = tags[0].getY();
                distance = tags[0].getDistance();
                camera.pixelToCentimeters(xc, yc, distance);
                P_d = R_dc.inverse().times(camera.P);
                P_o = R_do.inverse().times(P_d);
                tag = true;
                distanceChart = P_o.get(2, 0);
            }
        }

        @Override
        public void trackersSend(TrackerData trackersData) {

        }

        @Override
        public void receivedPerformanceData(VisionPerformance d) {

        }

        @Override
        public void receivedRawData(float[] vision_raw) {

        }

        @Override
        public void receivedData(VisionData d) {

        }

        @Override
        public void receivedVisionOf(float[] of_dx, float[] of_dy) {

        }

        @Override
        public void typeDetected(int detection_camera_type) {

        }

    };

    @Override
    public void activate(IARDrone drone) {
        this.drone = drone;
        drone.getNavDataManager().addAttitudeListener(attitudeListener);
        drone.getNavDataManager().addVisionListener(visionListener);
    }

    @Override
    public void deactivate() {
        drone.getNavDataManager().removeAttitudeListener(attitudeListener);
        drone.getNavDataManager().removeVisionListener(visionListener);
    }

    @Override
    public String getTitle() {
        return "Distance Chart";
    }

    @Override
    public String getDescription() {
        return "Displays a chart with the latest distance";
    }

    @Override
    public boolean isVisual() {
        return true;
    }

    @Override
    public Dimension getScreenSize() {
        return new Dimension(330, 250);
    }

    @Override
    public Point getScreenLocation() {
        return new Point(330, 390);
    }

    @Override
    public JPanel getPanel() {
        return this;
    }

    private AttitudeListener attitudeListener = new AttitudeListener() {

        @Override
        public void attitudeUpdated(float pitch, float roll, float yaw) {
            pitchchart = pitch;
            rollchart = roll;
        }

        @Override
        public void attitudeUpdated(float pitch, float roll) {

        }

        @Override
        public void windCompensation(float pitch, float roll) {

        }
    };
    
    public void SaveCSV() throws IOException{
        writer = new CSVWriter(new FileWriter(csv));
        writer.writeAll(data); 
        writer.close();
    }
}
